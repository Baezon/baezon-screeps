//main.labManager.RESOURCE_GROUPS.wide10[main.hives.Gamma.resourceGroup]

var REACTIONS_REV = {};
_.forEach(REACTIONS, (arr, r1) => _.forEach(arr, (out, r2) => REACTIONS_REV[out] = [r1, r2]));
RESOURCES = _.clone(RESOURCES_ALL);
RESOURCES.splice(RESOURCES.indexOf('G'), 1);
RESOURCES.splice(RESOURCES.indexOf('UL')+1, 0, 'G');
RESOURCES_REV = _.clone(RESOURCES).reverse();

REACTION_COST = {};
RESOURCES.forEach(res => {
    var react = REACTIONS_REV[res];
    REACTION_COST[res] = react ?
        REACTION_COST[react[0]] + REACTION_COST[react[1]] + 1 : 0;
});

function addTo(obj, i, val) {
    if (i in obj) obj[i] += val;
    else obj[i] = val;
    if (!obj[i]) delete obj[i];
}

var RESOURCE_GROUPS = {};
RESOURCE_GROUPS.wide3 = _.values(_.map(REACTIONS_REV, (In, out) => In.concat([out])));
RESOURCE_GROUPS.wide6 = _.values(_.map(REACTIONS_REV, (In, out) => In.concat(Array(4).fill(out))));
RESOURCE_GROUPS.wide10 = _.values(_.map(REACTIONS_REV, (In, out) => In.concat(Array(8).fill(out))));
RESOURCE_GROUPS.dense6 = [
    ['H','O','OH','K','KH','KH2O'],
    ['H','O','OH','K','KHO2','KO'],
    ['H','O','OH','Z','ZH','ZH2O'],
    ['H','O','OH','Z','ZHO2','ZO'],
    ['H','O','OH','U','UH','UH2O'],
    ['H','O','OH','U','UHO2','UO'],
    ['H','O','OH','L','LH','LH2O'],
    ['H','O','OH','L','LHO2','LO'],
    ['H','O','OH','G','GH','GH2O'],
    ['H','O','OH','G','GHO2','GO'],
    ['K','Z','U','L','ZK','UL'],
    ['H','O','OH','G','ZK','UL'],
    ['X','XKH2O',null,'XKHO2','KHO2','KH2O'],
    ['X','XZH2O',null,'XZHO2','ZHO2','ZH2O'],
    ['X','XUH2O',null,'XUHO2','UHO2','UH2O'],
    ['X','XLH2O',null,'XLHO2','LHO2','LH2O'],
    ['X','XGH2O',null,'XGHO2','GHO2','GH2O'],
];
RESOURCE_GROUPS.dense10 = [
    ['H','O','OH','K','KH','KH2O','X','XKH2O'],
    ['H','O','OH','K','KO','KHO2','X','XKHO2'],
    ['H','O','OH','Z','ZH','ZH2O','X','XZH2O'],
    ['H','O','OH','Z','ZO','ZHO2','X','XZHO2'],
    ['H','O','OH','U','UH','UH2O','X','XUH2O'],
    ['H','O','OH','U','UO','UHO2','X','XUHO2'],
    ['H','O','OH','L','LH','LH2O','X','XLH2O'],
    ['H','O','OH','L','LO','LHO2','X','XLHO2'],
    ['H','O','OH','G','GH','GH2O','X','XGH2O'],
    ['H','O','OH','G','GO','GHO2','X','XGHO2'],
    ['H','O','OH','G','ZK','UL','K','Z','U','L'],
];

var GROUP_REACTIONABLE = _.mapValues(RESOURCE_GROUPS, groups => groups.map(group =>
    group.filter(out => REACTIONS_REV[out] && REACTIONS_REV[out].every(input => group.includes(input)))));
var GROUP_INPUTS = _.mapValues(RESOURCE_GROUPS, (groups, gs) => groups.map((group, i) =>
    group.filter(input => GROUP_REACTIONABLE[gs][i].some(out => REACTIONS_REV[out].includes(input)))));
module.exports = {
    RESOURCE_GROUPS,
    GROUP_REACTIONABLE,
    GROUP_INPUTS,
    addTo,
    transfer: function(from, to, mineral, amount) {
        if (!amount) return;
        //console.log("transfer "+amount+" "+mineral+" from "+from+" to "+to);
        var arr = this.transfers[mineral] || (this.transfers[mineral] = {});
        arr = arr[from] || (arr[from] = {});
        addTo(arr, to, amount);
        addTo(this.surplus[from], mineral, -amount);
        addTo(this.surplus[to], mineral, amount);
    },
    react: function(hive, mineral, amount) {
        if (!(mineral in REACTIONS_REV) || !amount) return;
        var arr = this.reactions[hive] || (this.reactions[hive] = {});
        var inputs = this.reactionInputs[hive] || (this.reactionInputs[hive] = {});
        addTo(arr, mineral, amount);
        //addTo(this.surplus[hive], mineral, amount);
        REACTIONS_REV[mineral].forEach(reactant => {
            addTo(this.surplus[hive], reactant, -amount);
            addTo(this.surplusAll, reactant, -amount);
            addTo(inputs, reactant, amount);
        });
    },
    calc: function(main) {
        var bigHives = _.values(main.hives).filter(hive =>
            hive.room && hive.room.storage && hive.room.terminal);
        var labHives = bigHives.filter(hive => hive.labType);
        this.surplus = {};
        this.surplusAll = {};
        this.transfers = {};
        this.reactions = {};
        this.reactionInputs = {};
        if (!labHives.length) return;
        //var excesses = {};
        bigHives.forEach((hive) => {
            var room = hive.room;
            room.find(FIND_STRUCTURES, {filter: {structureType: STRUCTURE_LAB}}).forEach((lab) => {
                addTo(hive.bulletin.store, RESOURCE_ENERGY, lab.energy);
                if (lab.mineralAmount)
                    addTo(hive.bulletin.store, lab.mineralType, lab.mineralAmount);
            });
            this.surplus[hive.name] = _.clone(hive.bulletin.store);
            /*_.forEach(hive.bulletin.store, (amt, res) => {
                if ((hive.resourceMax[res] || 0) < amt)
                    (excesses[res] || (excesses[res] = [])).push(hive);
            });*/
            _.forEach(hive.resourceMax, (amt, res) => addTo(this.surplus[hive.name], res, -amt));
            _.forEach(hive.outbox, (order) => addTo(this.surplus[hive.name], order.res, -order.amt));
            //main.log("store = "+s(hive.bulletin.store), hive);
            //main.log("surplus = "+s(this.surplus[hive.name]), hive);
            _.forEach(this.surplus[hive.name], (amt, res) => addTo(this.surplusAll, res, amt));
        });

        RESOURCES_REV.forEach((res) => {
            if (this.surplusAll[res] >= 0) return;
            var labWork = Math.ceil(-this.surplusAll[res] / labHives.length);
            _.forEach(labHives, (lab) => this.react(lab.name, res, labWork));
        });

        _.forEach(main.hiveDistance, o => {
            var s0 = this.surplus[o[0]], s1 = this.surplus[o[1]];
            if (!s0 || !s1) return;
            _.forEach(s0, (v0, res) => {
                var v1 = s1[res];
                var hiveSrc, hiveDst, amt;
                if (v0 > 0 && v1 < 0) {
                    hiveSrc = o[0];
                    hiveDst = o[1];
                    amt = Math.min(v0, -v1);
                } else if (v0 < 0 && v1 > 0) {
                    hiveSrc = o[1];
                    hiveDst = o[0];
                    amt = Math.min(v1, -v0);
                } else return;
                this.transfer(hiveSrc, hiveDst, res, amt);
            });
        });

        //bigHives.forEach((hive) =>
        //    main.log("end surplus = "+s(this.surplus[hive.name]), hive));

        //_.forEach(this.transfers, (arr, res) =>
        //    console.log("transfers."+res+" = "+s(arr)));

        //_.forEach(this.transfers, (v,res) => console.log("transfers:"+res+" = "+s(v)));
        //_.forEach(this.reactions, (v,hive) => console.log("reactions:"+hive+" = "+s(v)));
        //this.transfers.energy = {};
    },

    runLabs: function(main, hive) {
        if (!hive.labType) return;
        hive.labs = hive.labs.map((lab) => Game.getObjectById(lab.id));
        if (!hive.labs.every(lab => lab)) hive.initLabs();
        var groupIdx = hive.resourceGroup == undefined ? this.calcResourceGroup(main, hive) : hive.resourceGroup;
        var group = RESOURCE_GROUPS[hive.labType][groupIdx];
        hive.labs.forEach((lab, i) => {
            if (lab.cooldown) return;
            var out = group[i];
            if (!out || !(out in REACTIONS_REV)) return;
            var ready = true;
            var [lab1, lab2] = REACTIONS_REV[out].map(input => {
                var j = group.indexOf(input);
                var inLab = hive.labs[j];
                if (j == -1 || inLab.mineralType != input) ready = false;
                return inLab;
            });
            if (ready) lab.runReaction(lab1, lab2);
        });
    },

    calcResourceGroup: function(main, hive) {
        if (!hive.labType) return;
        var storage = hive.room && hive.room.storage;
        if (!storage) return;
        var groups = RESOURCE_GROUPS[hive.labType];
        var excess = (res, diff) => Math.max((storage.store[res] || 0) + diff - (hive.resourceMax[res] || 0));
        var excessDiff = (res, diff) => excess(res, diff) - excess(res, 0);
        var score = i => {
            var baseScore = _.sum(GROUP_REACTIONABLE[hive.labType][i], out => {
                var amt = Math.min(
                    _.min(REACTIONS_REV[out].map(input => hive.bulletin.store[input] || 0)),
                    this.reactions[hive.name] && this.reactions[hive.name][out] || 0);
                return amt - excessDiff(out, amt) - _.sum(REACTIONS_REV[out], input => excessDiff(input, -amt));
            });
            var misfilled = hive.labs.filter((lab, j) => lab.mineralAmount && lab.mineralType != groups[i][j]).length;
            return Math.max(baseScore - misfilled * 5000, baseScore / (1 + .1 * misfilled) | 0);
        }
        var newGroup = +_.max(Object.keys(groups), score);
        /*if (hive.name == 'Beta') {
            main.log("parts: "+Object.keys(groups).map(i => s([groups[i], score(i), _.map(GROUP_REACTIONABLE[hive.labType][i], out => [out,
                    _.min(REACTIONS_REV[out].map(input => hive.bulletin.store[input] || 0)),
                    this.reactions[hive.name] && this.reactions[hive.name][out] || 0])])).join('\n'), hive);
            main.log("scores: "+s(_.map(Object.keys(groups), i => [groups[i],score(i)])), hive);
        }*/
        if (hive.resourceGroup && newGroup != hive.resourceGroup) {
            main.log("switching resource group " + groups[hive.resourceGroup] +
             " score " + score(hive.resourceGroup) + " => " + groups[newGroup] + " score " + score(newGroup), hive);
        }
        return hive.resourceGroup = newGroup;
    },
};
