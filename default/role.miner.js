var base = require('role.worker');

module.exports = {
    run: function(main, creep) {
        base.throe(main, creep);

        var deposit = creep.memory.deposit;
        if (!deposit) return;
        var cache = main.roomCache(deposit.room);
        var obj = cache.deposits[deposit.id];
        if (!obj) {
            main.log("deposit = "+s(deposit), creep);
            main.log("cache.deposits = "+s(cache.deposits), creep);
            return;
        }
        var targetLoc = obj.pos;
        var struct = obj.container && cache.structures[obj.container];
        if (struct) {
            //console.log(Game.getObjectById(obj.container) + " " + obj.container);
            targetLoc = new RoomPosition(struct.x, struct.y, deposit.room);
            if (!creep.pos.isEqualTo(targetLoc))
                return creep.pathify(targetLoc);
        } else if (targetLoc.roomName != creep.room.name) {
            return creep.pathify(targetLoc);
        } else if (obj.container) {
            main.clearRoomCache(deposit.room);
            main.roomCache(deposit.room);
            main.log("clearing room cache "+deposit.room+", inconsistent container for "+obj.pos, creep, 1);
            return creep.pathify(targetLoc);
        } else if (!creep.pos.isNearTo(targetLoc))
            return creep.pathify(targetLoc);

        var source = Game.getObjectById(deposit.id);
        if (!source) {
            main.log("source does not exist!", creep);
            return;
        }
        base.goAndDo(creep, 'harvest', source, (t) => creep.harvest(t),
            [ERR_NOT_ENOUGH_RESOURCES, ERR_TIRED, ERR_INVALID_TARGET], {timeout: () => creep.suicide()});
    }
}
