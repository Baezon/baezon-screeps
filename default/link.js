
module.exports = {
    run: function(main, hive) {
        var min = 400;
        var room = hive.room;
        var links = room.find(FIND_STRUCTURES, {filter: {structureType: STRUCTURE_LINK}});
        var sources = [], dests = [];
        links.forEach((link) =>
            (room.storage && link.pos.getRangeTo(room.storage.pos) <= 2 ? sources : dests).push(link));
        sources.forEach((src) => {
            if (src.cooldown > 0) return;
            var dest = _.max(dests, (link) => link.energyCapacity - link.energy);
            if (dest) {
                var amt = Math.min(src.energy, dest.energyCapacity - dest.energy);
                if (amt >= src.energyCapacity/2) src.transferEnergy(dest);
            }
        })
    }
};
