// TODO: Source Keeper mining!
// TODO: Boosting!
// TODO: Power harvesting!
// TODO: Automatic market code!
console.log("compiling");

var roles = {};
['reserver','worker','defender','custom','miner','carrier','upgrader', 'signer']
    .forEach((name) => roles[name] = require('role.'+name));
var Squad = require('squad');
var Hive = require('hive');
var tower = require('tower');
var pathifier = require('pathifier');
var link = require('link');
var labManager = require('labManager');
var terminal = require('terminal');
var stats = require('stats');
var errors = [];

var simMode = Game.rooms.sim != undefined;
var privateServer = !(3 in Game.map.describeExits('W0N0'));
if (!simMode) {
    s = JSON.stringify;
    require('util');
}
var jobReporting = "Epsilon"; //"Beta";
var hives = simMode ?
    {sim: new Hive('sim')} : privateServer ?
    {Omega: new Hive('W8N7', {domain: ['W9N7'], numWorkers: 2, wallStrength: 3000000}),
     Psi: new Hive('W9N9', {numWorkers: 4})} :
    {Alpha: new Hive('W64S21', {domain: ['W65S21','W63S21'], highways: ['W63S22'], guards: [],
      numWorkers: 3, wallStrength: 6e6, labType: 'wide10'}),
     Beta: new Hive('W64S22', {domain: ['W64S23', 'W65S22', 'W65S23'],
      guards: [],
      numWorkers: 3, wallStrength: 6e6, labType: 'wide10'}),
     Gamma: new Hive('W61S22', {domain: ['W62S22', 'W61S23','W61S21'],
      numWorkers: 3, wallStrength: 6e6, labType: 'wide10'}),
     Delta: new Hive('W63S24', {domain: ['W63S25', 'W62S24','W63S23'],
      guards: [/*{layout: ["dismantler","dismantler","dismantler","healer","healer","healer"], color: COLOR_WHITE, leadTime: 0, budget: 5000},*/,],
      numWorkers: 3, wallStrength: 6e6}),
     Epsilon: new Hive('W66S23', {domain: ['W67S22','W67S23','W67S24'],
      guards: [],
      numWorkers: 4, wallStrength: 6e6}),
     Zeta: new Hive('W69S23', {domain: ['W68S23','W69S22','W69S24'],
      numWorkers: 3, numCarriers: 1, wallStrength: 4e6}),
     Eta: new Hive('W61S26', {domain: ['W61S25'],
      numWorkers: 3, wallStrength: 2e6}),
     Theta: new Hive('W67S21', {domain: ['W66S21'],
      numWorkers: 3, numCarriers: 1, wallStrength: 1e6}),
     Iota: new Hive('W67S25', {
      numWorkers: 2})};

var ALLIES = ["Baezon",
    "Atavus","BlackLotus","Ashburnie","Tanjera","Pantek59","Moria","seancl","Atlan",
    "Finndibaen","Klapaucius","ChaosDMG","Kenshi","Maxion","Trepidimous","Plasticbag",
    "Lomewilwarin","joshua00214","vestad","andrwmorph","Hachima","Calame",
    "DoofenShmirtz","Atanner","Sheeo","Optiminx"];

var hiveParents = {};
_.forEach(hives, (hive, name) => hive.init(hiveParents, name));

var hiveDistance = _.sortBy([].concat.apply([], _.map(hives, hive => _.map(hives, hive2 =>
    hive.name <= hive2.name && [hive.name, hive2.name]).filter(o => o))),
    o => Game.map.getRoomLinearDistance(hives[o[0]].roomName, hives[o[1]].roomName, true));

var CONTROLLER_ACCUM = Array(9).fill(0);
for (var i = 1; i < 8; i++)
    CONTROLLER_ACCUM[i+1] = CONTROLLER_ACCUM[i] + CONTROLLER_LEVELS[i];

var shouldDonate;

function creepBodyCost(body) {
    var cost = 0;
    body.forEach((type) => cost += BODYPART_COST[typeof type == 'string' ? type : type.type]);
    return cost;
}

function creepBody(begin, middle, end, limit, ilimit) {
    var iterCost = creepBodyCost(begin) + creepBodyCost(middle);
    var baseCost = creepBodyCost(end);
    if (iterCost == 0) return end;
    var iter = (limit - baseCost) / iterCost | 0;
    if (iter < 1) iter = 1;
    if (iter > ilimit) iter = ilimit;
    if (iter*(begin.length+middle.length)+end.length > 50)
        iter = (50 - end.length) / (begin.length+middle.length) | 0;
    var ret = [];
    for (var i = 0; i < iter; i++) begin.forEach((a) => ret.push(a));
    for (var i = 0; i < iter; i++) middle.forEach((a) => ret.push(a));
    end.forEach((a) => ret.push(a));
    return ret;
}

Object.defineProperty(Room.prototype, "hive", {
    get: function hive() {
        var name = hiveParents[this.name];
        return name && hives[name];
    }
});

Object.defineProperty(RoomPosition.prototype, "hive", {
    get: function hive() {
        var name = hiveParents[this.roomName];
        return name && hives[name];
    }
});

Object.defineProperty(Structure.prototype, "energy", {
    get: function energy() {
        switch (this.structureType) {
            case STRUCTURE_CONTAINER:
            case STRUCTURE_TERMINAL:
            case STRUCTURE_STORAGE:
                return this.store.energy;
        }
    }
});

Object.defineProperty(Creep.prototype, "hive", {
    get: function hive() {
        if(!this.memory.hive) this.memory.hive = hiveParents[this.room.name] || 'Alpha';
        return hives[this.memory.hive];
    }
});

String.prototype.hashCode = function(){
	var hash = 0;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		char = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}
Structure.prototype.maxTransferAmount = function(res) {
    res = res || RESOURCE_ENERGY;
    switch (this.structureType) {
        case STRUCTURE_CONTAINER:
        case STRUCTURE_TERMINAL:
        case STRUCTURE_STORAGE:
            return this.storeCapacity - _.sum(this.store);
        case STRUCTURE_EXTENSION:
        case STRUCTURE_SPAWN:
        case STRUCTURE_LINK:
        case STRUCTURE_TOWER:
            return res == RESOURCE_ENERGY ? this.energyCapacity - this.energy : 0;
        case STRUCTURE_NUKER:
            switch (res) {
                case RESOURCE_ENERGY: return this.energyCapacity - this.energy;
                case RESOURCE_GHODIUM: return this.ghodiumCapacity - this.ghodium;
                default: return 0;
            }
        case STRUCTURE_LAB:
            return res == RESOURCE_ENERGY ? this.energyCapacity - this.energy :
                !this.mineralType ? this.mineralCapacity :
                res == this.mineralType ? this.mineralCapacity - this.mineralAmount : 0;
        default: return 0;
    }
};

module.exports = {
    hives,
    hiveParents,
    hiveDistance,
    creepBody,
    creepBodyCost,
    Squad,
    jobReporting,
    ALLIES,
    CONTROLLER_ACCUM,
    labManager,
    MILITARY_ROLES: ['melee', 'healer', 'defender', 'dismantler', 'tank', 'garrison'],

    decodeErr: function(err) {
        switch (err) {
            case OK: return 'OK';
            case ERR_NOT_OWNER: return 'ERR_NOT_OWNER';
            case ERR_NO_PATH: return 'ERR_NO_PATH';
            case ERR_NAME_EXISTS: return 'ERR_NAME_EXISTS';
            case ERR_BUSY: return 'ERR_BUSY';
            case ERR_NOT_FOUND: return 'ERR_NOT_FOUND';
            case ERR_NOT_ENOUGH_ENERGY: return 'ERR_NOT_ENOUGH_ENERGY';
            case ERR_NOT_ENOUGH_RESOURCES: return 'ERR_NOT_ENOUGH_RESOURCES';
            case ERR_INVALID_TARGET: return 'ERR_INVALID_TARGET';
            case ERR_FULL: return 'ERR_FULL';
            case ERR_NOT_IN_RANGE: return 'ERR_NOT_IN_RANGE';
            case ERR_INVALID_ARGS: return 'ERR_INVALID_ARGS';
            case ERR_TIRED: return 'ERR_TIRED';
            case ERR_NO_BODYPART: return 'ERR_NO_BODYPART';
            case ERR_NOT_ENOUGH_EXTENSIONS: return 'ERR_NOT_ENOUGH_EXTENSIONS';
            case ERR_RCL_NOT_ENOUGH: return 'ERR_RCL_NOT_ENOUGH';
            case ERR_GCL_NOT_ENOUGH: return 'ERR_GCL_NOT_ENOUGH';
            default: return err;
        }
    },
    asRoomPosition: function(obj) { return new RoomPosition(obj.x, obj.y, obj.roomName); },
    partEffectiveness: function(part) {
        return {
            UH: 2, KO: 2, LO: 2, ZH: 2, GO: 1/.7,
            UH2O: 3, KHO2: 3, LHO2: 3, ZH2O: 3, GHO2: 1/.5,
            XUH2O: 4, XKHO2: 4, XLHO2: 4, XZH2O: 4, XGHO2: 1/.3,
        }[part.boost] || 1;
    },
    unemploy: function(name, hive) {
        delete Game.creeps[name].memory.job;
        if (hive) Game.creeps[name].memory.hive = hive;
    },
    job: function(name) {
        var mem = Game.creeps[name].memory;
        var title = mem.role;
        var pos;
        switch (mem.role) {
            case 'worker':
            case 'carrier':
                var job = mem.job;
                if (!job) return 'unemployed ' + mem.role;
                title = job.type;
                switch (job.type) {
                    case 'harvester': if (job.deposit) pos = job.deposit.pos; break;
                    case 'repairer': if (job.current) pos = Game.getObjectById(job.current).pos; break;
                    case 'builder': pos = Game.getObjectById(job.site).pos; break;
                    case 'extensionFilling': return (job.permanent ? 'permanent ':'')+'extension filler';
                    case 'transfer':
                        switch(job.subType) {
                            case 'minerRetrieval': return 'miner retriever for ' + Game.getObjectById(job.withdrawTarget).pos;
                            case 'energyRemoval': return 'energy remover for ' + Game.getObjectById(job.withdrawTarget).pos;
                            case 'garbageCollection': return 'garbage collector of ' + Game.getObjectById(job.withdrawTarget).pos;
                            case 'terminalFilling': return 'terminal filler from ' + Game.getObjectById(job.withdrawTarget).pos;
                            case 'terminalEmptying': return 'terminal emptier';
                            case 'labTending':
                                var from = Game.getObjectById(job.withdrawTarget);
                                var to = Game.getObjectById(job.transferTarget);
                                return from.structureType == STRUCTURE_LAB ? 'lab emptier for ' + from.pos : 'lab filler for ' + to.pos;
                            case 'shipping': return 'shipping from ' + this.hiveParents[Game.getObjectById(job.withdrawTarget).room.name];
                            default: return "carrier transfer " + job.type
                                + " from " + Game.getObjectById(job.withdrawTarget).pos
                                + " to " + Game.getObjectById(job.transferTarget).pos;
                        }
                    default: pos = job.pos; break;
                } break;
            case 'miner':
                var cache = this.roomCache(mem.deposit.room);

                pos = cache.structures[cache.deposits[mem.deposit.id].container].pos;
                break;
            case 'reserver': return title + " for " + mem.room;
        }
        return pos ? title + " for " + RoomPosition.prototype.toString.call(pos) : title;
    },

    jobs: function(hive) {
        var creeps = _.values(Game.creeps);
        if (hive) creeps = creeps.filter((creep) => creep.memory.hive == hive);
        return creeps.map((creep) => creep.name + ": " + this.job(creep.name)).join("\n");
    },

    repairs: function(name) {
        var job;
        if (name) job = Game.creeps[name].memory.job;
        else _.forEach(Game.creeps, (creep, name) => {
            if (creep.memory.job && creep.memory.job.type == 'repairer') {
                job = creep.memory.job;
                console.log("Repairer: "+creep.name);
            }
        });
        if (job && job.type == 'repairer') {
            return job.list.map(Game.getObjectById).map((struct) => struct.structureType + " at " + struct.pos).join("\n");
        }
    },
    squad: function(name) {
        if (!name) return Memory.squads.map((sq, i) => i+": "+s(sq)).join('\n');
        var out;
        Memory.squads.some((squad) => {
            if (squad.creeps.includes(name)) {
                out = squad;
                return true;
            }
        });
        return out;
    },

    sMerge: function(first, ...merge) {
        var target = this.squad(first);
        merge.forEach((to) => Memory.squads = Memory.squads.filter((squad) => {
            if (!squad.creeps.includes(to)) return true;
            target.creeps = target.creeps.concat(squad.creeps);
            target.layout = target.layout.concat(squad.layout);
        }));
        return target;
    },

    sSplit: function(...names) {
        var obj = typeof names[names.length-1] == "string" ? {} : names.pop();
        if (typeof obj == "number") obj = {flagColor: obj};
        var copy = Object.assign(new Squad(this.squad(names[0])), obj);
        var creeps = names.map(name => Game.creeps[name]).filter(creep => creep);
        copy.creeps = creeps.map(creep => creep.name);
        copy.layout = creeps.map(creep => creep.memory.role);
        Memory.squads.forEach((squad) => squad.creeps =
            squad.creeps.filter(name => !copy.creeps.includes(name)));
        Memory.squads.push(copy);
        return copy;
    },

    /*analyzeMarket: function(res) {
        Game.market.getAllOrders((o) => o.resourceType == res);
    },*/

    log: function(msg, ctx, unimportance) {
        var type = ctx instanceof Creep ? "creep" : ((ctx instanceof Hive  || typeof ctx === 'string' )? "hive" : undefined);
        var reportEvenThoughUnimportant = jobReporting &&
            (jobReporting ==
                (type == "creep" ? ctx.hive.name :
                (type == "hive" ? (ctx.name || ctx) : false)));
        if(!unimportance || (unimportance && reportEvenThoughUnimportant)) {
            switch (type) {
                case "hive":
                    console.log("Hive " + (ctx.name || ctx) + ": " + msg);
                    break;
                case "creep":
                    console.log(ctx.memory.role + " " + ctx.name + ", Hive " + ctx.memory.hive + " at " + ctx.pos + ": " + msg);
                    break;
                default: console.log(msg);
            }
        }
    },

    error: function(msg) {
        errors.push(msg instanceof Error ? msg : new Error(msg));
    },

    guard: function(f) {
        try { return f() }
        catch(e) { errors.push(e); }
    },

    loop: function () {
        if (!simMode) main = this;
        Game.main = this;
        errors.length = 0;
        PathFinder.use(true);

        for (var name in Memory.creeps) {
            if (!Game.creeps[name]) {
                delete Memory.creeps[name];
                this.log('Held a funeral for ' + name);
            }
        }

        _.forEach(hives, (hive) => hive.clearBulletin());
        _.forEach(Game.creeps, (creep) => {
            var hive = creep.hive;
            if (hive) hive.bulletin.populate(creep);
        });
        this.guard(() => labManager.calc(this));

        Memory.stats.delays = 0;

        _.forEach(Game.creeps, (creep) => {
            if(!creep.spawning && creep.memory.role in roles) {
                this.guard(() => roles[creep.memory.role].run(this, creep));
            }
        });

        var squads = this.squads = (Memory.squads || []).map((obj) => { if (obj) {
            var squad = new Squad(obj);
            return this.guard(() => squad.run(this)) && squad;
        }}).filter((obj) => obj);

        _.forEach(hives, (hive) => this.guard(() => {
            var room = hive.room;
            if (!room) return;
            var towers = room.find(FIND_STRUCTURES,
                {filter: (s) => s.structureType == STRUCTURE_TOWER});
            this.guard(() => tower.run(this, hive, towers));
            this.guard(() => link.run(this, hive));
            this.guard(() => terminal.run(this, hive));
            this.guard(() => labManager.runLabs(this, hive));

            this.guard(() => _.forEach(hive.outbox, (order, i) => {
                if(hive.room.terminal.store[order.res] >= order.amt) {
                    this.log("Attempting to " + (order.gift ? "send " + order.amt + " " + order.res : "deal"), hive);
                    if(order.gift) {
                        var result = this.room.terminal.send(order.res, order.amt, order.dest, description);
                        if (result == OK) {
                            hive.outbox.splice(i,1);
                            labManager.addTo(hive.outboxRes, order.res, -order.amt);
                            return this.log("OK", hive);
                        } else return this.log(main.decodeErr(result),hive);
                    } else {
                        var marketOrder = Game.market.getOrderById(order.id);
                        if(!marketOrder) {
                            hive.outbox.splice(i,1);
                            labManager.addTo(hive.outboxRes, order.res, -order.amt);
                            return this.log("Cannot find order");
                        }
                        var result = this.room.terminal.deal(order.id, order.amt, hive.roomName);
                        if (result == OK) {
                            hive.outbox.splice(i,1);
                            labManager.addTo(hive.outboxRes, order.res, -order.amt);
                            return this.log("OK", hive);
                        } else return this.log(main.decodeErr(result),hive);
                    }

                }
            }));

            var attackSize = 0;
            hive.hiveAndDomain.forEach((name) => {
                var room = Game.rooms[name];
                if(room) {
                    room.find(FIND_HOSTILE_CREEPS, {filter: (c) => c.owner && c.owner.username != "Invader"}).forEach((creep) => creep.body.forEach((part) => {
                        switch (part.type) {
                            case ATTACK:
                            case RANGED_ATTACK:
                            /*case WORK:*/
                                attackSize += this.partEffectiveness(part)*.1;
                                break;
                            case HEAL:
                                attackSize += this.partEffectiveness(part);
                                break;
                        }
                    }));
                }
            });
            if (attackSize > towers.length*15) {
                hive.alertMode = attackSize;
                if(true || room.controller.activateSafeMode() != OK) {
                    var defenderSize = _.sum(squads, (squad) =>
                        squad.hive == hive.name && squad.garrison == hive.name ? squad.creeps.length*30 : 0);
                    this.log("Alert: " + attackSize + ", " + defenderSize, hive);
                    /*while (defenderSize < attackSize) {
                        var squad = new Squad(hive.name, COLOR_YELLOW, ['defender','defender','healer']);
                        squad.garrison = hive.name;
                        squads.push(squad);
                        defenderSize += 90;
                    }*/
                    var assistants = _.values(hives).filter((assist) =>
                        assist.room.storage && assist.name != hive.name && assist.distanceTo(hive) <= 4);
                    assistants.forEach((assist) => {
                        var defenderSize = _.sum(squads, (squad) =>
                            squad.hive == assist.name && squad.garrison == hive.name ? squad.creeps.length*30 : 0);
                        while (defenderSize < attackSize/assistants.length) {
                            var squad = new Squad(assist.name, COLOR_YELLOW, ['melee']);
                            squad.garrison = hive.name;
                            squads.push(squad);
                            defenderSize += 30;
                        }
                    });
                }
            } else delete hive.alertMode;


            hive.domain.some((name) => {
                var room = Game.rooms[name];
                if(!room  || room.find(FIND_HOSTILE_CREEPS).length) {
                    if (!squads.some((squad) => squad.hive == hive.name && squad.garrison)) {
                        var squad = new Squad(hive.name, COLOR_BROWN, Array(hive.room.controller.level <= 5 ? 2 : 1).fill('garrison'), 3000);
                        squad.garrison = hive.name;
                        squads.push(squad);
                    }
                    return true;
                }
            });

            hive.guards.forEach((guard, i) => {
                if (!squads.some((squad) => squad.hive == hive.name && squad.guard == i &&
                    (squad.building || squad.ticksToLive > (guard.leadTime || 0)))) {
                    var squad = new Squad(hive.name, guard.color, guard.layout, guard.budget);
                    this.log("Making guard "+s(guard), hive);
                    squad.guard = i;
                    squads.push(squad);
                }
                return true;
            });
        }));

        if(Game.time%50 == 0) this.guard(() => this.updateStructureCache());

        //if(Game.time%300 == 0) {
        //    shouldDonate = true;
        //}

        var doneHives = {};
        _.forEach(Game.spawns, (spawn) => {
            var name = hiveParents[spawn.room.name];
            if (!(name in doneHives) && typeof this.guard(() => hives[name].buildCreep(this, spawn)) == 'string')
                doneHives[name] = true;
        });

        Memory.squads = squads;

        this.guard(() => require('visual').loop());
        this.guard(() => stats.collect_stats());

        if (errors.length) {
            if (errors.length == 1) throw errors[0];
            var s = "";
            errors.forEach(e => s += e.stack+"\n");
            var e = new Error();
            e.name = '';
            e.message = s;
            throw e;
        }
    },

    isAlly: function(obj) {
        return obj && obj.owner && ALLIES.includes(obj.owner.username);
    },

    filterNonAllies: function(arr) {
        return _.filter(arr, c => !this.isAlly(c));
    },

    tryMilitaryActions: function(creep, flagHostile) {
        var tryAttack = () => {
            if(!creep.getActiveBodyparts(ATTACK)) return false;
            var hostile = this.filterNonAllies(creep.pos.findInRange(FIND_HOSTILE_CREEPS, 1))[0] ||
                (flagHostile && flagHostile.pos.getRangeTo(creep.pos) <= 1 ? flagHostile : undefined) ||
                this.filterNonAllies(creep.pos.findInRange(FIND_HOSTILE_STRUCTURES, 1))[0] ||
                (!this.isAlly(creep.room.controller) &&
                creep.pos.findInRange(FIND_STRUCTURES, 1,
               {filter: (s) => s.structureType == STRUCTURE_WALL})[0]);
            if (hostile) {
                creep.attack(hostile);
                return true;
            }
            return false;
        }

        var tryDismantle = () => {
            if(!creep.getActiveBodyparts(WORK)) return false;
            var hostile;
            if (flagHostile && flagHostile.pos.getRangeTo(creep.pos) <= 1) {
                hostile = flagHostile;
            }
            hostile = hostile ||
                this.filterNonAllies(creep.pos.findInRange(FIND_HOSTILE_STRUCTURES, 1))[0] ||
                (!this.isAlly(creep.room.controller) &&
                creep.pos.findInRange(FIND_STRUCTURES, 1,
               {filter: (s) => s.structureType == STRUCTURE_WALL})[0]);
            if (hostile) {
                creep.dismantle(hostile);
                return true;
            }
            return false;
        }

        var tryHeal = () => {
            if(!creep.getActiveBodyparts(HEAL)) return false;
            var list = creep.pos.findInRange(FIND_CREEPS, 1, {filter: (c) => this.isAlly(c) && c.hits != c.hitsMax});
            if (list.length) {
                creep.heal(_.min(list, (creep) => creep.hits/creep.hitsMax))
                return true;
            }
            return false;
        }

        var tryRangedAttack = () => {
            if(!creep.getActiveBodyparts(RANGED_ATTACK)) return false;
            var hostiles = this.filterNonAllies(creep.pos.findInRange(FIND_HOSTILE_CREEPS, 3)
                .concat(creep.pos.findInRange(FIND_HOSTILE_STRUCTURES, 3)));
            if(hostiles.length > 1) {
                creep.rangedMassAttack();
                return true;
            } else if(hostiles.length == 1) {
                creep.rangedAttack(hostiles[0]);
                return true;
            }
            return false;
        }

        var tryRangedHeal = () => {
            if(!creep.getActiveBodyparts(HEAL)) return false;
            var list = creep.pos.findInRange(FIND_CREEPS, 3, {filter: (c) => this.isAlly(c) && c.hits != c.hitsMax});
            if (list.length) {
                creep.rangedHeal(_.min(list, (creep) => creep.hits/creep.hitsMax))
                return true;
            }
            return false;
        }

        switch (creep.memory.role) {
        case 'defender':
        case 'garrison':
        case 'melee':
            return tryAttack() ? tryRangedAttack() :
                tryRangedAttack() ? tryHeal() : tryHeal() || tryRangedHeal() || tryDismantle();

        case 'healer':
            return tryHeal() ? tryRangedAttack() : tryRangedHeal() ||
                (tryAttack() ? tryRangedAttack() : tryRangedAttack() || tryDismantle());

        case 'dismantler':
            return tryDismantle() && (tryAttack() ? tryRangedAttack() :
                tryRangedAttack() ? tryHeal() : tryHeal() || tryRangedHeal());
        }
    },

    hiveChar: function(hive,lower) {
        if(hive[0] == "W") {
            hive = hiveParents[hive];
        }
        var letter = {Alpha: 'Α', Beta: 'Β', Gamma: 'Γ', Delta: 'Δ', Epsilon: 'Ε',
            Zeta: 'Ζ', Eta: 'Η', Theta: 'Θ', Iota: 'Ι', Kappa: 'Κ', Lambda: 'Λ',
            Mu: 'Μ', Nu: 'Ν', Xi: 'Ξ', Omicron: 'Ο', Pi: 'Π', Rho: 'Ρ', Sigma: 'Σ',
            Tau: 'Τ', Upsilon: 'Υ', Phi: 'Φ', Chi: 'Χ', Psi: 'Ψ', Omega: 'Ω'}[hive];
        return letter ? (lower ? letter.toLowerCase() : letter) : 'character not found';
    },

    roomCache: function(name) {
        if (!Memory.roomCache) Memory.roomCache = {};
        if (Memory.roomCache[name]) return Memory.roomCache[name];
        var room = Game.rooms[name]; if (!room) return;
        var structures = {};
        room.find(FIND_STRUCTURES).forEach(s => {
            structures[s.id] = {type: s.structureType, x: s.pos.x, y: s.pos.y};
        });
        room.find(FIND_CONSTRUCTION_SITES).forEach(s => {
            structures[s.id] = {type: s.structureType, x: s.pos.x, y: s.pos.y, site: true};
        });
        var deposits = {};
        room.find(FIND_SOURCES)
            .concat(room.find(FIND_STRUCTURES, {filter: {structureType: STRUCTURE_EXTRACTOR}})
            .map((s) => s.pos.lookFor(LOOK_MINERALS)[0])).forEach(source => {
                var pos = source.pos;
                var container = pos.findInRange(FIND_STRUCTURES, 1,
                    {filter: s => s.structureType == STRUCTURE_CONTAINER &&
                        !s.pos.isNearTo(s.room.controller.pos)})[0];
                if (!container) {
                    var containerSite = pos.findInRange(FIND_CONSTRUCTION_SITES, 1,
                        {filter: s => s.structureType == STRUCTURE_CONTAINER &&
                            !s.pos.isNearTo(s.room.controller.pos)})[0];
                    if (containerSite) structures[containerSite.id].source = source.id;
                }
                deposits[source.id] = {
                    room: name,
                    pos,
                    capacity: source.room.lookForAtArea(LOOK_TERRAIN,
                        pos.y-1, pos.x-1, pos.y+1, pos.x+1, true).filter((o) => o.terrain != 'wall').length,
                    container: container && (structures[container.id].source = source.id, container.id),
                    distance: pathifier.pathfind(hives[hiveParents[name]].center.pos, pos, 1).path.length,
                };
            });
        return Memory.roomCache[name] = {deposits, structures};
    },

    roomCacheAddStructure: function(s) {
        var cache = this.roomCache(s.room.name);
        var out = {type: s.structureType, x: s.pos.x, y: s.pos.y};
        if (s instanceof ConstructionSite) out.site = true;
        if (s.structureType == STRUCTURE_CONTAINER) {
            var source = (!s.pos.isNearTo(s.room.controller.pos) && s.pos.findInRange(FIND_SOURCES, 1)[0]) ||
                s.pos.findInRange(FIND_STRUCTURES, 1, {filter: {structureType: STRUCTURE_EXTRACTOR}})
                    .map((s) => s.pos.lookFor(LOOK_MINERALS)[0])[0];
            if (source) {
                out.source = source.id;
                if (!out.site) cache.deposits[source.id].container = s.id;
            }
        }
        return cache.structures[s.id] = out;
    },

    getDeposit: function({id, room}) {
        return this.roomCache(room).deposits[id];
    },

    clearRoomCache: function(name) {
        delete Memory.roomCache[name];
    },

    updateStructureCache: function() {
        //var check = (arr) => ('58b50eeaeca4385774ef094e' in arr);
        _.forEach(Memory.roomCache, (obj, roomName) => {
            //if (this.name == 'Theta' && roomName == 'W67S21') console.log("in "+roomName+"? "+check(obj.structures)+" "+s(Memory.roomCache).hashCode())
            Object.keys(obj.structures).forEach(id => {
                if (Game.getObjectById(id)) return;
                var o = obj.structures[id];
                var pos = new RoomPosition(o.x, o.y, roomName);
                if (!Game.rooms[roomName]) return;
                var finish = (newId) => {
                    delete obj.structures[id];
                    if (o.source) obj.deposits[o.source].container = newId;
                }
                var flag = pos.lookFor(LOOK_FLAGS, {filter: flag => flag.color == COLOR_RED})[0];
                if (flag || o.type == "constructedWall") {
                    finish();
                    if(flag) flag.remove();
                    this.log("removed red-flag " + o.type + " at " + pos, this);
                    return;
                }
                var newStruct = pos.lookFor(LOOK_STRUCTURES,
                    {filter: s => s.structureType == o.type})[0];
                if (newStruct) {
                    delete o.site;
                    obj.structures[newStruct.id] = o;
                    finish(newStruct.id);
                } else {
                    var newSite = pos.lookFor(FIND_CONSTRUCTION_SITES,
                        {filter: s => s.structureType == o.type})[0];
                    if (newSite) {
                        o.site = true;
                        obj.structures[newSite.id] = o;
                        finish();
                    } else if (pos.createConstructionSite(o.type) == OK) {
                        this.log(o.type + " lost and recreated at " + pos, hives[hiveParents[roomName]]);
                    } else
                        this.log("Unable to create construction site for " + o.type + " at " + pos, hives[hiveParents[roomName]]);
                }
            });
            //if (this.name == 'Theta' && roomName == 'W67S21') console.log("done, in "+roomName+"? "+check(obj.structures)+" "+s(Memory.roomCache).hashCode())
        });
    },

}
