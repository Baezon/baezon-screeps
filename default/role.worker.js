module.exports = {
    throe: function(main, creep) {
        if (creep.ticksToLive == 1) {
            main.log(/*"RIP: "+new Error().stack*/"RIP", creep);
        }
    },

    start: function(main, creep) {
        this.throe(main, creep);

        if (!creep.memory.job) {
            creep.hive.assignJob(creep);
            if(creep.memory.job) main.log("got a job as a " + main.job(creep.name), creep, 1);
        }
        return creep.memory.job;
    },

    goAndDo: function(creep, name, target, f, exclude, opts) {
        var main = require('main');
        /*if (creep.room.name != target.pos.roomName) {
            creep.pathify(target.pos.roomName);
        }*/
        var result = target.pos.getRangeTo(creep.pos) > 3 ? ERR_NOT_IN_RANGE : f(target);
        if (result == ERR_NOT_IN_RANGE) {
            result = creep.pathify(target, 1);
            if (result == OK || result == ERR_TIRED) result = ERR_NOT_IN_RANGE;
        }
        else delete creep.memory.path;
        if (result != OK && result != ERR_NOT_IN_RANGE && !(exclude && exclude.includes(result))) {
            if (result == ERR_NO_BODYPART)
                (opts && opts.nobody || (() => creep.suicide()))();
            main.log(name+" failed: "+main.decodeErr(result) + ", " + creep.memory.timeout, creep);
            if(creep.memory.timeout >= 30) {
                main.log("gave up", creep);
                (opts && opts.timeout || (() => delete creep.memory.job))();
                creep.memory.timeout = 0;
            } else creep.memory.timeout += 1;
        } else creep.memory.timeout = 0;
        return result;
    },

    energyNeed: function(main, creep, exclude) {
        var hive = creep.hive;
        var valid = (s) => (!exclude || !exclude.includes(s.id)) && s && !s.pos.lookFor(LOOK_FLAGS).length;
        var closest = (types, max) => creep.pos.findClosestByRange(FIND_STRUCTURES,
                {filter: (s) => types.includes(s.structureType) && valid(s)
                    && s.energy < max(s)});
        var out;
        if (hive.alertMode) out =
            closest([STRUCTURE_TOWER], s => .5*s.energyCapacity) ||
            closest([STRUCTURE_SPAWN, STRUCTURE_EXTENSION], s => s.energyCapacity) ||
            closest([STRUCTURE_TOWER], s => s.energyCapacity);
        else out =
            closest([STRUCTURE_LINK], s => s.pos.getRangeTo(s.room.controller) > 1 ? s.energyCapacity : 0) ||
            // REMOVE LATER ^^^
            closest([STRUCTURE_SPAWN, STRUCTURE_EXTENSION], s => s.energyCapacity) ||
            closest([STRUCTURE_TOWER], s => s.energyCapacity);
        if (out) return out;
        var out = (hive.room.storage && hive.room.storage.energy > 50000 && closest([STRUCTURE_TERMINAL], s => 100000)) ||
            closest([STRUCTURE_NUKER], s => 300000) ||
            closest([STRUCTURE_LINK, STRUCTURE_LAB],
                s => s.pos.getRangeTo(s.room.controller) > 1 ? s.energyCapacity : 0) ||
            creep.pos.findClosestByRange(hive.upgradeContainers.map(Game.getObjectById),
                {filter: (s) => valid(s) && s.energy < s.storeCapacity*.8});
        return out;
    },

    getEnergy: function(main,creep) {
        var hive = creep.hive;
        if (creep.room.name != hive.room.name) {
            creep.pathify(hive.room);
            return;
        }
        var source = (hive.room.storage && hive.room.storage.store.energy >= 10000) ?
            hive.room.storage : creep.pos.findInRange(FIND_SOURCES,1)[0] ||
            _.min(creep.room.find(FIND_SOURCES), (s) => s.pos.findInRange(FIND_CREEPS,2).length);
        if (!source) {
            return ERR_NOT_ENOUGH_ENERGY;
        }
        if (source instanceof Structure) return this.goAndDo(creep, 'withdraw', source, (t) => creep.withdraw(t, RESOURCE_ENERGY));
        else return this.goAndDo(creep, 'harvest', source, (t) => creep.harvest(t));
    },

    unloadEnergy: function(main,creep) {
        var hive = creep.hive;
        var job = creep.memory.job;
        if (creep.room.name != hive.room.name) {
            creep.pathify(hive.room);
            return;
        }
        var energyDumpTarget = job.energyDumpTarget ? Game.getObjectById(job.energyDumpTarget) : undefined;
        if (energyDumpTarget
            && energyDumpTarget.energy == energyDumpTarget.energyCapacity) energyDumpTarget = undefined;
        energyDumpTarget = energyDumpTarget || this.energyNeed(main, creep) ||
            creep.room.storage || (hive.room.find(FIND_STRUCTURES, {filter: {structureType: STRUCTURE_SPAWN}}).length ? undefined : hive.room.find(FIND_CONSTRUCTION_SITES)[0]);
        if(energyDumpTarget) job.energyDumpTarget = energyDumpTarget.id;

        if(!energyDumpTarget) creep.pathify(creep.room.controller);
        if (this.goAndDo(creep, 'transfer', energyDumpTarget ? energyDumpTarget : creep.room.controller,
            (t) => energyDumpTarget ? (energyDumpTarget instanceof ConstructionSite ?
                creep.build(t) : creep.transfer(t, RESOURCE_ENERGY)) : creep.upgradeController(t)) == OK &&
            energyDumpTarget && !(energyDumpTarget instanceof ConstructionSite) &&
            creep.carry.energy > energyDumpTarget.maxTransferAmount())
            if (energyDumpTarget = this.energyNeed(main, creep, [energyDumpTarget.id]))
                this.goAndDo(creep, 'move', energyDumpTarget, () => ERR_NOT_IN_RANGE);
    },

    quitJob: function(msg, creep) {
        if(msg) main.log(msg, creep, 1);
        delete creep.memory.job;
        return;
    },

    run: function(main, creep) {
        var job = this.start(main, creep);

        if((Math.random()*16) > (Game.cpu.bucket/600)) {
            if(!Memory.stats.delays) Memory.stats.delays = 0;
            Memory.stats.delays++;
            return;
        }

        creep.memory.unemployed = creep.memory.unemployed || 0;
        if (!job) {

            creep.memory.unemployed++;
            if(creep.memory.unemployed%20 == 0) main.log("Hey, I haven't gotten a job in a while.", creep);
            return;
        } else creep.memory.unemployed = 0;

        var hive = creep.hive;

        switch (job.type) {
        case 'harvester':
            if (job.deposit) {
                if (_.sum(creep.carry) == creep.carryCapacity) delete job.deposit;
                else {
                    var deposit = Game.getObjectById(job.deposit.id);
                    if (!deposit && creep.room.name != job.deposit.room) {
                        creep.pathify(main.getDeposit(job.deposit).pos);
                        return;
                    }
                    else if (!deposit) {
                        return this.quitJob("Quit harvest job. Deposit not found. This is what I had => " + (s(job.deposit)), creep);
                    }
                    var result = this.goAndDo(creep, 'harvest', deposit,
                        (t) => creep.harvest(t), [ERR_NOT_ENOUGH_RESOURCES]);
                    if (result == ERR_NOT_ENOUGH_RESOURCES) {
                        delete job.deposit;
                    }
                }
            }
            if (!job.deposit) {
                if (creep.carry.energy == 0) {
                    return this.quitJob("Quit harvest job. Energy dropped off.", creep, 1);
                }
                this.unloadEnergy(main,creep);
            }
            break;

        case 'builder':
            var site = Game.getObjectById(job.site);
            if (!site) {
                if (job.pos) {
                    require('pathifier').clearCostMatrix(job.pos.roomName);
                    //hive.clearRoomCache(job.pos.roomName);
                }
                this.quitJob("Quit build job. Site is gone.", creep, 1);
                return;
            }
            if (!job.deploying) {
                if (creep.carry.energy >= site.progressTotal - site.progress || _.sum(creep.carry) == creep.carryCapacity) job.deploying = true;
                else if (this.getEnergy(main,creep) == ERR_NOT_ENOUGH_ENERGY) {
                    return this.quitJob("Quit build job. Couldn't find energy for a build job.", creep, 1);
                }
            }
            if (job.deploying) {
                if (creep.carry.energy == 0) {
                    delete creep.memory.job;
                    return;
                }
                creep.pathify(site);
                this.goAndDo(creep, 'build', site, (t) => creep.build(t));
            }
            break;

        case 'repairer':
            var struct;
            if (job.current) {
                struct = Game.getObjectById(job.current);
                if (!struct || hive.isOptimal(struct) || struct.pos.lookFor(LOOK_FLAGS).length) {
                    delete job.current;
                }
            }
            if (!job.current) {
                var objs = job.list.map((id) => Game.getObjectById(id)).filter((o) => o && !hive.isOptimal(o));
                struct = creep.pos.findClosestByRange(objs) || objs.length && objs[0];
                job.list = objs.map((o) => o.id);
                if (!struct) {
                    return this.quitJob("Quit repair job. Repair list finished.", creep);
                }
                job.current = struct.id;
            }
            if (!job.deploying) {
                if (_.sum(creep.carry) == creep.carryCapacity) job.deploying = true;
                else if (this.getEnergy(main,creep) == ERR_NOT_ENOUGH_ENERGY) {
                    return this.quitJob("Quit repair job. Couldn't find energy.", creep);
                }
            }
            if (job.deploying) {
                if (creep.carry.energy == 0) {
                    job.deploying = false;
                    return;
                }
                this.goAndDo(creep, 'repair', struct, (t) => creep.repair(t));
            }
            break;

        case 'donate':
            var targetRoom = job.targetRoom;
            if(creep.room.name != targetRoom){
                creep.pathify(targetRoom);
            }else if(creep.pos.getRangeTo(Game.spawns.Del) > 5){
                creep.pathify(Game.spawns.Del);
            }else{
                delete creep.memory.job;
            }
            break;

        case 'upgrader':
            if(!job.deploying) {
                if (_.sum(creep.carry) != creep.carryCapacity && this.getEnergy(main,creep) == ERR_NOT_ENOUGH_ENERGY) {
                    return this.quitJob("Quit upgrading job. Couldn't find energy.", creep);
                } else job.deploying = true;
            }
            if (job.deploying) {
                if (this.goAndDo(creep, 'upgrade', creep.hive.room.controller,
                    (t) => creep.upgradeController(t), [ERR_NOT_ENOUGH_ENERGY]) == ERR_NOT_ENOUGH_ENERGY) {
                    return this.quitJob("Quit upgrading job. Upgrade finished, ran out of energy.", creep);
                }
            }
            break;

        case 'dismantler':
            var target = Game.getObjectById(job.target);
            if (!target) {
                return this.quitJob("Quit dismantling job. Target is gone.", creep);
            }
            if (!job.deploying) {
                if (_.sum(creep.carry) == creep.carryCapacity) {
                    job.deploying = true;
                } else {
                    this.goAndDo(creep, 'dismantle', target, (t) => creep.dismantle(target));
                }
            } else {
                if (_.sum(creep.carry) == creep.carryCapacity) {
                    this.unloadEnergy(main,creep);
                } else {
                    return this.quitJob("Quit dismantling job. Dismantled some, dropped off energy.", creep);
                }
            }
            break;

        case 'emergencyRepairer':
            var target = _.min(creep.memory.job.pos.findInRange(FIND_STRUCTURES, 2, {filter: (s) => s.structureType == STRUCTURE_RAMPART || s.structureType == STRUCTURE_WALL}), (s) => s.hits);
            if (!job.deploying) {
                if (_.sum(creep.carry) == creep.carryCapacity) job.deploying = true;
                else if (this.getEnergy(main,creep) == ERR_NOT_ENOUGH_ENERGY) {
                    return this.quitJob("Quit emergency repair job. Couldn't find energy.", creep);
                }
            }
            if (job.deploying) {
                if (creep.carry.energy == 0) {
                    return this.quitJob("Quit emergency repair job. Repaired some, ran out of energy.", creep);
                }
                this.goAndDo(creep, 'repair', target, (t) => creep.repair(t));
            }
            break;

        case 'wallStrengthener':
            var wall = Game.getObjectById(job.wall);
            if (!wall) {
                return this.quitJob("Quit wall strengthening job. Couldn't find the wall.", creep);
            }
            if (!job.deploying) {
                if (_.sum(creep.carry) == creep.carryCapacity) job.deploying = true;
                else if (this.getEnergy(main,creep) == ERR_NOT_ENOUGH_ENERGY) {
                    return this.quitJob("Quit wall strengthening job. Repaired some, ran out of energy.", creep);
                }
            }
            if (job.deploying) {
                if (creep.carry.energy == 0) {
                    return this.quitJob("Quit wall strengthening job. Repaired some, dropped off energy.", creep);
                }
                creep.pathify(wall);
                this.goAndDo(creep, 'repair', wall, (t) => creep.repair(t));
            }
            break;

        case 'transfer':
        case 'extensionFilling':
            require('role.carrier').run(main, creep);
            break;



        default:
            main.log("What the fuck is this job "+job.type+"?", creep);
            break;
        }
    }
}
