/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.attacker');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function(main, creep) {
        main.tryMilitaryActions(creep);
        var targetRoom = creep.memory.targetRoom;
        if (!targetRoom) {
            main.log("targetRoom is null!", creep);
            return;
        }
        if (creep.room.name != targetRoom) {
            creep.pathify(targetRoom);
        } else {
            var target = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if (target) {
                var rampart = target.pos.findClosestByRange(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_RAMPART && !s.pos.lookFor(LOOK_CREEPS).length});
                creep.pathify(rampart || target);
            } else {
                var center = main.hiveParents[targetRoom].center;
                if (center && creep.pos.getRangeTo(center) >= 5)
                    creep.pathify(center);
            }
        }
    }

};
