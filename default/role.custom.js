/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.custom');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function(main, creep) {
        var targetRoom = "W69S23";
        var deposit = Game.getObjectById(creep.memory.deposit);
        if(creep.room.name != targetRoom) {
           creep.pathify(targetRoom);
        } else {
            var dep = creep.memory.deploying;
            if(!dep){
                if(creep.carry.energy == creep.carryCapacity) {
                    creep.memory.deploying = true;
                }
                else{
                    if(creep.harvest(deposit) == ERR_NOT_IN_RANGE){
                        creep.pathify(deposit);
                    }
                }
            }else{
                if(creep.carry.energy == 0) creep.memory.deploying = false;
                else{
                    var struct = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES)
                    if(creep.build(struct) == ERR_NOT_IN_RANGE){
                        creep.pathify(struct);
                    }
                }
            }
        }
    }
};
