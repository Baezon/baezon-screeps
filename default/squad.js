var pathifier = require('pathifier');
/*Use:
Memory.squads.push(new main.Squad('Delta', COLOR_WHITE, ['melee', 'dismantler', 'dismantler', 'healer', 'healer', 'healer', 'healer']))
*/

function Squad(hive, flagColor, layout, budget) {
    if (typeof hive == 'string') {
        this.hive = hive;
        this.flagColor = flagColor;
        this.layout = layout;
        this.creeps = layout ? Array(layout.length) : [];
        this.building = true;
        this.mode = 'muster';
        this.budget = budget;
    } else {
        Object.assign(this, hive);
        /*var obj = hive;
        this.hive = obj.hive;
        this.flagColor = obj.flagColor;
        this.layout = obj.layout;
        this.creeps = obj.creeps;
        this.building = obj.building;
        this.mode = obj.mode;
        this.garrison = obj.garrison;
        this.budget = obj.budget;*/
    }
};

Squad.prototype = {
    build: function(main, budget, test) {
        if (!this.building) return;
        if(test)console.log('test '+JSON.stringify(this.creeps));
        for (var i in this.layout) {
            if(test)console.log('i= '+i+" "+this.creeps[i]);
            if (!this.creeps[i]) {
                if(test)console.log('test '+this.layout[i]);
                switch (this.layout[i]) {
                    case 'defender': return [i, main.creepBody([MOVE], [RANGED_ATTACK, ATTACK], [], budget)];
                    case 'melee': return [i, main.creepBody([], [ATTACK, MOVE], [], budget)];
                    case 'garrison': return [i, main.creepBody([ATTACK,ATTACK,ATTACK], [HEAL,MOVE,MOVE,MOVE,MOVE], [], budget)];
                    case 'tank': return [i, main.creepBody([TOUGH], [MOVE], [], budget)];
                    case 'dismantler': /*return [i, main.creepBody([TOUGH],[WORK,MOVE], [],budget)];*/
                        var body = [];
                        var numWorks = (budget - 1180) / 90 | 0;
                        if (numWorks < 0) numWorks = 0;
                        if (numWorks > 25) numWorks = 25;
                        for (var j = 0; j < 25-numWorks; j++) body.push(TOUGH);
                        for (var j = 0; j < 25; j++) body.push(MOVE);
                        for (var j = 0; j < numWorks; j++) body.push(WORK);
                        return [i, body];
                    case 'flashDismantler':
                        var body = [];
                        for (var j = 0; j < 6; j++) body.push(MOVE);
                        for (var j = 0; j < 20; j++) body.push(WORK);
                        return [i, body];
                    case 'healer': return [i, main.creepBody([MOVE], [HEAL], [], budget)]
                    case 'patroller':
                        var body = [];
                        for (var j = 0; j < 35; j++) body.push(RANGED_ATTACK);
                        for (var j = 0; j < 13; j++) body.push(MOVE);
                        for (var j = 0; j < 2; j++) body.push(HEAL);
                        return [i, body];
                    default: console.log("Unknown layout"); return;
                }
            }
        }
    },
    receive: function(i, name) {
        this.creeps[i] = name;
    //    if (this.creeps.every((name) => name && !name.spawning))
    //        this.building = 1;
    },

    get leader() {
        var leader;
        this.creeps.some((name) => leader = Game.creeps[name]);
        return leader;
    },

    log: function(msg) {
        console.log((this.garrison ?
            "Garrison " + this.hive + " - " :
            "Squad ") + this.leader.name + ": " + msg);
    },

    setMode: function(mode, reason) {
        this.log("mode "+ this.mode + " => " + (this.mode = mode)
            + (reason? " - " + reason:''));
    },

    get ticksToLive() {
        return _.min(this.creeps.map((name) => {
            var creep = Game.creeps[name];
            return creep ? creep.ticksToLive : 0;
        }));
    },

    run: function(main, restarts) {
        var leader, room, flagPos, musterFlag, flagHostile, imperfect, nearby = true, flashReady = true,
            allClear = true, restart, creepPos = {}, aliveCreeps = [];
        this.creeps.forEach((name, i) => {
            var creep = Game.creeps[name];
            if (!creep || creep.spawning) {
                imperfect = true;
                if (!this.building) this.creeps[i] = null;
                return;
            }
            if (!leader) {
                leader = creep;
            }
            aliveCreeps.push(creep);
        });
        if (restarts > 3) {
            this.log("restart stack overflow");
            return leader;
        }
        aliveCreeps.forEach((creep) => {
            creepPos[creep.id] = creep.pos;
        });
        aliveCreeps.forEach((creep) => {
            if (leader == creep) {
                room = creep.room;
                if(this.mode == 'muster') {
                    if (this.guard != undefined) {
                        var lairs = creep.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_KEEPER_LAIR});
                        if (lairs.length) musterFlag = _.min(lairs, (lair) => lair.ticksToSpawn).pos;
                    }
                    musterFlag = musterFlag || creep.pos.findClosestByRange(FIND_FLAGS,
                        {filter: (flag) => flag.color == COLOR_PURPLE || flag.color == this.flagColor});
                }
                var flags = [];
                _.forEach(Game.flags, (flag, id) => {
                    if(flag.color == this.flagColor) {
                        flags.push(flag);
                    }
                });
                flags.sort((a,b) => a.pos.getRangeTo(creep.pos) - b.pos.getRangeTo(creep.pos));
                flags.some((flag) => {
                    if (flag.room) return flagHostile =
                        main.filterNonAllies(flag.pos.lookFor(LOOK_STRUCTURES))[0] ||
                        main.filterNonAllies(flag.pos.lookFor(LOOK_CONSTRUCTION_SITES))[0];
                });
                flagPos = flags.length && flags[0].pos;
                if (!flagPos && this.garrison) {
                    var hive = main.hives[this.garrison];
                    if(leader.memory.role == 'defender') {
                        var room = hive.room;
                        if (room) {
                            var creeps = room.find(FIND_HOSTILE_CREEPS);
                            if (creeps.length) return flagPos = creeps[0].pos;
                        }
                    } else {
                        hive.hiveAndDomain.some((roomName) => {
                            var room = Game.rooms[roomName];
                            if (room) {
                                var creeps = room.find(FIND_HOSTILE_CREEPS);
                                if (creeps.length) return flagPos = creeps[0].pos;
                            }
                        });
                        if (!flagPos) hive.hiveAndDomain.some((roomName) => {
                            if (!Game.rooms[roomName])
                                return flagPos = new RoomPosition(25,25,roomName);
                        });
                    }
                    room = hive.room;
                    if (!flagPos && room) {
                        flags = room.find(FIND_FLAGS,
                            {filter: (flag) => flag.color == COLOR_PURPLE || flag.color == this.flagColor})
                        flagPos = flags.length && flags[0].pos;
                    }
                }
            }

            main.tryMilitaryActions(creep, flagHostile);

            if (creep.memory.role == 'flashDismantler' && !(flagHostile && flagHostile.pos.getRangeTo(creep.pos) <= 1))
                flashReady = false;

            switch (this.mode) {
            case 'muster':
                creep.pathify(musterFlag || leader, musterFlag ? 0 : 1, {type: 'mil', creepPos});
                var pathObj = creep.memory.path;
                /*if (creep.name == "Lauren") main.log(s([creep.pos,
                    musterFlag ? musterFlag.pos : leader.pos, pathifier.squadMuster(creep.pos,
                        musterFlag ? musterFlag.pos : leader.pos, true)]), creep);*/
                nearby = nearby && (!pathObj || pathObj.path.length <= 3);
                break;

            case 'move':
                if (creep != leader) {
                    creep.pathify(leader, 1, {type: 'mil', creepPos});
                    var pathObj = creep.memory.path;
                    nearby = nearby && (!pathObj || pathObj.path.length <= 3+this.creeps.length);
                    break;
                }
                else if (!flagPos) {
                    this.setMode('attack', 'no flag');
                    return restart = true;
                }
                else if (flagPos.roomName != creep.room.name) break;
                else {
                    this.setMode('attack', 'arrived at target room');
                    return restart = true;
                }/* {
                    var path = creep.room.findPath(creep.pos, flagHostile ? flagHostile.pos : flagPos);
                    creep.moveByPath(path);
                    if (path.length <= 5 && this.mode == 'move') {
                        this.setMode('attack');
                    } else break;
                }*/

            case 'attack':
                function pathify(goal, range) {
                    creep.pathify(goal, range, {type: 'mil', creepPos});
                    var pathObj = creep.memory.path;
                    if (pathObj) creepPos[creep.id] = pathObj.nextPos;
                }
                /*if (!flagPos) {
                    this.setMode('muster', 'no flag');
                    creep.moveTo(musterFlag.pos);
                }
                else*/if (flagPos && flagPos.roomName != creep.room.name) {
                    pathify(flagPos);
                    allClear = false;
                }
                else if (flagHostile && creep.memory.role != 'healer') {
                    pathify(flagHostile, flagHostile instanceof ConstructionSite ? 0 : 1);
                    allClear = false;
                }
                else {
                    var target;
                    switch (creep.memory.role) {
                        case 'garrison':
                        case 'patroller':
                        case 'melee':
                            target = (!creep.room.controller || !creep.room.controller.safeMode || creep.room.controller.my) &&
                            (creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {filter: (creep) => !main.isAlly(creep) &&
                                 _.sum([ATTACK, RANGED_ATTACK, HEAL], p => creep.getActiveBodyparts(p)) > 0}) ||
                                 creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES,
                                     {filter: (s) => !main.isAlly(s) && s.structureType != STRUCTURE_CONTROLLER}) ||
                                  creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {filter: (creep) => !main.isAlly(creep)}));
                            break;
                        case 'defender':
                            if (!creep.room.controller || !creep.room.controller.safeMode || creep.room.controller.my) {
                                var targets = creep.room.find(FIND_HOSTILE_CREEPS);
                                target = creep.pos.findClosestByRange(FIND_STRUCTURES,
                                    {filter: (s) => s.structureType == STRUCTURE_RAMPART &&
                                        !s.pos.lookFor(LOOK_CREEPS).length && targets.some((tgt) => tgt.pos.getRangeTo(s.pos) <= 1)}) ||
                                    creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {filter: (creep) => !main.isAlly(creep) &&
                                        _.sum([ATTACK, RANGED_ATTACK, WORK], p => creep.getActiveBodyparts(p)) > 0}) ||
                                    creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {filter: (creep) => !main.isAlly(creep)});
                            }
                            break;
                        case 'dismantler':
                            target = (!creep.room.controller || !creep.room.controller.safeMode || creep.room.controller.my) &&
                            (creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES,
                                {filter: (s) => !main.isAlly(s) && s.structureType != STRUCTURE_CONTROLLER}) ||
                             creep.pos.findClosestByRange(FIND_STRUCTURES,
                                {filter: (s) => s.structureType == STRUCTURE_WALL}));
                            break;
                        case 'healer':
                            var list = creep.room.find(FIND_CREEPS, {filter: (c) => main.isAlly(c) && c.hits != c.hitsMax});
                            target = list.length ? _.min(list, (creep) => creep.hits/creep.hitsMax) : leader;
                            break;
                    }
                    if (target) {
                        if (creep.memory.role != 'healer') {
                            allClear = false;
                        }
                        //if (creep.memory.role == 'healer' || this.creeps.some((name) =>
                        //    Game.creeps[name] && Game.creeps[name].memory.role == 'healer') && Game.time % 3 != 0) {
                        pathify(creepPos[target.id] || target, target.structureType == STRUCTURE_RAMPART ? 0 : 1);
                    }
                }
                break;

            case 'recycle':
                var spawn = this.recycle && Game.getObjectById(this.recycle) ||
                    main.hives[this.hive].room.find(FIND_STRUCTURES, {filter:{structureType: STRUCTURE_SPAWN}})[0];
                if (spawn) {
                    this.recycle = spawn.id;
                    require('role.worker').goAndDo(creep, 'recycle', spawn, (t) => t.recycleCreep(creep));
                }
                break;
            }
        });
        if (restart) return this.run(main, (restarts || 0) + 1);
        if (flagHostile && flashReady) {
            this.creeps.forEach((name) => {
                var creep = Game.creeps[name];
                if (creep && !creep.spawning && creep.memory.role == 'flashDismantler')
                    this.log(creep.name+" dismantle: "+main.decodeErr(creep.dismantle(flagHostile)));
            });
        }
        if (this.building && !imperfect) {
            delete this.building;
            this.log("moving out");
        }
        if (!leader) return this.building;
        switch (this.mode) {
            case 'muster':
                if (!this.building && nearby && flagPos && flagPos.roomName != leader.room.name) {
                    this.setMode('move', 'moving out');
                    //if (flag) flag.remove();
                }
                if(leader.room.find(FIND_HOSTILE_CREEPS,{filter: (c) => !main.isAlly(c)}).length || leader.room.find(FIND_HOSTILE_STRUCTURES,
                    {filter: (s) => !main.isAlly(s) && s.structureType != STRUCTURE_CONTROLLER}).length) {
                    allClear = false;
                    this.setMode('attack', 'hostiles in the room');
                    return this.run(main, (restarts || 0) + 1);
                }
                break;
            case 'move':
                if (!flagPos) this.setMode('muster', 'no flag');
                else if (flagPos.roomName != leader.room.name && (nearby || Game.time %2 == 0)) {
                    leader.pathify(flagPos, 0, {type: 'mil', creepPos, maxOps: 20000});
                }
                break;
            case 'attack':
                if (allClear) {
                    this.setMode('muster', 'no enemies');
                    return this.run(main, (restarts || 0) + 1);
                }
                break;
        }
        return leader;
    },
};

module.exports = Squad;
