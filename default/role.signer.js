var base = require('role.worker');

module.exports = {

    run: function(main, creep){
        var msg = creep.memory.msg;
        var targetRoom = creep.memory.targetRoom;
        if(creep.room.name != targetRoom) {
            creep.pathify(targetRoom);
        }else{
            if(base.goAndDo(creep, 'sign', creep.room.controller, (t)  => creep.signController(t, msg)) == OK) creep.suicide();
        }
    }

};
