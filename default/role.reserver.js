module.exports = {
    run: function(main, creep) {
        var room = Game.rooms[creep.memory.room];
        if (room) {
            var result = room.controller == Game.getObjectById('57ef9c7986f108ae6e60c3e1') ? creep.claimController(room.controller) :
                room.controller.owner ? creep.attackController(room.controller) : creep.reserveController(room.controller);
            if (result == ERR_NOT_IN_RANGE) {
                creep.pathify(room.controller, 1);
            }
            else {
                if (result == ERR_NO_BODYPART) creep.suicide();
                else if (result != OK) console.log(creep.name+" reservation failed: "+main.decodeErr(result));
                delete creep.memory.path;
            }
        } else {
            creep.pathify(creep.memory.room);
        }
    }
};
