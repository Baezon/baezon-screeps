var base = require('role.worker');

module.exports = {
    run: function(main,creep) {
        var job = base.start(main, creep);

        if(parseInt(creep.id[creep.id.length-1],16) > (Game.cpu.bucket/600)) {
            if(!Memory.stats.delays) Memory.stats.delays = 0;
            Memory.stats.delays++;
            return;
        }

        if (!job) return;
        switch (job.type) {

        case 'transfer':
            var withdrawTarget = Game.getObjectById(job.withdrawTarget);
            if (!withdrawTarget) {
                main.log("withdraw target dead for "+job.subType, creep);
                delete creep.memory.job;
                return;
            }

            var dumpTarget = Game.getObjectById(job.dumpTarget);
            if (dumpTarget) {
                if(base.goAndDo(creep, 'dump', dumpTarget, (t) => {
                    for (var res in creep.carry) {
                        var result = creep.transfer(t, res);
                        if (result != OK && result != ERR_NOT_ENOUGH_RESOURCES) return result;
                    }
                    return OK;
                }) != OK) return;
                delete job.dumpTarget;
            }
            if (!job.deploying) {
                if (!(withdrawTarget.store ? (job.resource ? withdrawTarget.store[job.resource] : _.sum(withdrawTarget.store)) : withdrawTarget.energy)) {
                    return base.quitJob("Quit transfer job. Withdraw target doesn't have the resource/any energy.", creep);
                }
                if (job.subType == 'garbageCollection') {
                    if (!creep.pos.isEqualTo(withdrawTarget.pos)) {
                        if (creep.pos.isNearTo(withdrawTarget.pos)) {
                            var creep2 = withdrawTarget.pos.lookFor(LOOK_CREEPS)[0];
                            if (creep2) {
                                creep2.moveTo(creep.pos);
                                if(creep2.memory && creep2.memory.path) delete creep2.memory.path;
                            }
                        }
                        creep.pathify(withdrawTarget);
                        return;
                    }
                    switch (base.goAndDo(creep,
                        'pickup', withdrawTarget, (t) => creep.pickup(t))) {
                        case OK: break;
                        default: return;
                    }
                }
                if (base.goAndDo(creep, 'withdraw', withdrawTarget, (t) => {
                    if (job.resource) return creep.withdraw(t, job.resource);
                    for (var res in t.store) {
                        var result = creep.withdraw(t, res);
                        if (result != OK && result != ERR_NOT_ENOUGH_RESOURCES) return result;
                    }
                    return OK;
                }) != OK) return;
                job.deploying = true;
            }
            var transferTarget = Game.getObjectById(job.transferTarget);
            var result = base.goAndDo(creep, 'transfer', transferTarget, (t) => {
                if (job.resource) return creep.transfer(t, job.resource);
                for (var res in t.store) {
                    var result = creep.transfer(t, res);
                    if (result != OK && result != ERR_NOT_ENOUGH_RESOURCES) return result;
                }
                return OK;
            }, [ERR_NOT_ENOUGH_ENERGY]);
            if (result != OK && result != ERR_NOT_ENOUGH_ENERGY) return;
            return base.quitJob("Quit transfer job. Transfer completed.", creep);

        case 'extensionFilling':
            var hive = creep.hive;
            if (!job.deploying) {
                var storage = hive.room.storage;
                if (!storage) {
                    delete creep.memory.job;
                    return;
                }
                if(base.goAndDo(creep, 'withdraw', storage, (t) => {
                    for (var res in creep.carry) {
                        var result = res == RESOURCE_ENERGY ?
                            creep.withdraw(t, res) : creep.transfer(t, res);
                        if (result != OK && result != ERR_FULL) return result;
                    }
                    return OK;
                }) != OK) return;
                job.deploying = true;
            }
            if (creep.carry.energy == 0) {
                if(job.permanent) {
                    job.deploying = false;
                } else return base.quitJob("Quit extension filling job. Filled some, ran out of energy.", creep);
                return;
            }

            var transferTarget = job.transferTarget ? Game.getObjectById(job.transferTarget) : undefined;
            var transferAmount = transferTarget ? transferTarget.maxTransferAmount() : 0;
            if (!transferAmount) transferTarget = undefined;
            transferTarget = transferTarget || base.energyNeed(main, creep);
            if (!transferTarget) {
                if (!job.permanent) return base.quitJob("Quit extension filling job. No more targets.", creep);
                return;
            }
            if (!transferAmount) transferAmount = transferTarget.maxTransferAmount();
            job.transferTarget = transferTarget.id;
            if (base.goAndDo(creep, 'transfer', transferTarget, (t) => creep.transfer(t, RESOURCE_ENERGY)) == OK && creep.carry.energy > transferAmount) {
                if (transferTarget = base.energyNeed(main, creep, [transferTarget.id])) {
                    base.goAndDo(creep, 'move', transferTarget, () => ERR_NOT_IN_RANGE);
                    job.transferTarget = transferTarget.id;
                }
            }
            break;

        default:
            main.log("What the fuck is this job "+job.type+"?", creep);
            break;
        }
    }
};
