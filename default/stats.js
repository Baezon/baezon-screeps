// Module to format data in memory for use with the https://screepspl.us
// Grafana utility run by ags131.
//
// Installation: Run a node script from https://github.com/ScreepsPlus/node-agent
// and configure your screepspl.us token and Screeps login (if you use Steam,
// you have to create a password on the Profile page in Screeps),
// then run that in the background (e.g., on Linode, AWS, your always-on Mac).
//
// Then, put whatever you want in Memory.stats, which will be collected every
// 15 seconds (yes, not every tick) by the above script and sent to screepspl.us.
// In this case, I call the collect_stats() routine below at the end of every
// trip through the main loop, with the absolute final call at the end of the
// main loop to update the final CPU usage.
//
// Then, configure a Grafana page (see example code) which graphs stuff whichever
// way you like.
//
// This module uses my resources module, which analyzes the state of affairs
// for every room you can see.


"use strict";
//const resources = require('resources');
//const cb = require('callback');

//global.stats_callbacks = new cb.Callback();

// Tell us that you want a callback when we're collecting the stats.
// We will send you in the partially completed stats object.
//function add_stats_callback(cbfunc) {
//    global.stats_callbacks.subscribe(cbfunc);
//}


// Update the Memory.stats with useful information for trend analysis and graphing.
// Also calls all registered stats callback functions before returning.
function collect_stats() {

    // Don't overwrite things if other modules are putting stuff into Memory.stats
    if (Memory.stats == null) {
        Memory.stats = { tick: Game.time };
    }


    // Note: This is fragile and will change if the Game.gcl API changes


    if(!Memory.stats.gcl) Memory.stats.gcl = {};

    Memory.stats.gcl.rate = Memory.stats.gcl.progress ? Game.gcl.progress - Memory.stats.gcl.progress : 0;

    Memory.stats.gcl.progress = Game.gcl.progress;
    Memory.stats.gcl.progressTotal = Game.gcl.progressTotal;

    function compounds(hive, exclude) {
        var storage = hive.room.storage;
        var out = 0;
        if(!storage) return;
        var total = 0;
        _.forEach(hive.resourceMax, (amt, res) => {
            if (exclude && exclude.includes(res)) return;
            total += amt;
            out += Math.min(storage.store[res] || 0, amt);
        });
//        Object.keys(hive.resourceMax).forEach((res) => {if(res != "energy") {console.log(res + " " + (s.store[res] || 0)); out += s.store[res] || 0}});
        return total ? out / total : 0;
    }

    var main = require('main');
    Memory.stats.creeps = 0;
    /*Memory.stats.costs = {
        minerCost: 0,
        carrierCost: 0,
        workerCost: 0,
        reserverCost: 0,
        rampartCost: 0,
        roadCost: 0,
        swampRoadCost: 0,
        containerCost: 0
    }*/
    Memory.stats.surplus = _.pick(main.labManager.surplusAll, (a,b) => b.length == 1);
    Memory.stats.resources = {U: 0, L: 0, K:0, Z:0, X:0, H:0, O:0};
    var baseMinerals = ["U","L","K","Z","X","H","O"];

    Memory.stats.hive = _.mapValues(main.hives, hive => {
        var spawns = hive.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_SPAWN});
        Memory.stats.creeps += hive.bulletin.numCreeps;
        /*var numRoads = 0;
        var numSwampRoads = 0;
        var numContainers = 0;
        hive.hiveAndDomain.forEach((name) => {
            var room = Game.rooms[name];
            if(!room) return;
            var roads = room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_ROAD});
            numSwampRoads += roads.filter((r) => Game.map.getTerrainAt(r.pos) == 'swamp').length;
            numRoads += roads.length;
            numContainers += room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_CONTAINER}).length
        });
        numRoads -= numSwampRoads;
        var minerCost = (hive.bulletin.roles.miner.length * 650)/1500; Memory.stats.costs.minerCost += minerCost;
        var carrierCost = (hive.bulletin.roles.carrier.length * 2400)/1500; Memory.stats.costs.carrierCost += carrierCost;
        var workerCost = (hive.bulletin.roles.worker.length * 3200)/1500; Memory.stats.costs.workerCost += workerCost;
        var reserverCost = (hive.bulletin.roles.reserver.length * 1100)/500; Memory.stats.costs.reserverCost += reserverCost;
        var rampartCost = hive.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_RAMPART}).length * 0.03; Memory.stats.costs.rampartCost += rampartCost;
        var roadCost = numRoads * 0.001; Memory.stats.costs.roadCost += roadCost;
        var swampRoadCost = numSwampRoads * 0.005; Memory.stats.costs.swampRoadCost += swampRoadCost;
        var containerCost = numContainers * .5; Memory.stats.costs.containerCost += containerCost;
        var theoreticalExpense = minerCost + carrierCost + workerCost + reserverCost + rampartCost + roadCost + swampRoadCost + containerCost;
        var theoreticalIncome = hive.deposits.length * 10;*/
        baseMinerals.forEach((res) => Memory.stats.resources[res] += (hive.room.storage ? hive.room.storage.store[res] || 0 : 0));
        var ramparts = hive.room.find(FIND_STRUCTURES, {filter: s => s.structureType == STRUCTURE_RAMPART});

        return {
            energy: hive.room.storage ? hive.room.storage.store[RESOURCE_ENERGY] : 0,
            energyAll: hive.bulletin.store.energy,
            creeps: hive.bulletin.numCreeps,
            compoundCompletion: compounds(hive, ['energy', 'H','O','U','K','L','Z','X']),
            level: Math.pow(hive.room.controller.level + (hive.room.controller.level == 8 ? 0 :
                hive.room.controller.progress/hive.room.controller.progressTotal), 2),
            progress: main.CONTROLLER_ACCUM[hive.room.controller.level] + hive.room.controller.progress,
            spawning: spawns.filter(s => s.spawning).length / spawns.length,
            labsUse: hive.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_LAB && s.cooldown}).length,
            wallStrength: Game.time % 10 ? (_.sum(ramparts, s => s.hits)/ramparts.length) : undefined,
            /*containerOverfill: Object.values(hive.bulletin.miners).filter((m) => Game.getObjectById(m).pos.lookFor(LOOK_STRUCTURES, {filter: s => s.structureType == STRUCTURE_CONTAINER})[0].energy == 2000).length,
            theoreticalExpense,
            theoreticalIncome,
            theoreticalProfit: theoreticalIncome - theoreticalExpense,*/
        };
    });

    //Memory.stats.hiveNames = Object.keys(main.hives).map(name => ({name, room: main.hives[name].hive}));

    const memory_used = RawMemory.get().length;
    // console.log('Memory used: ' + memory_used);
    Memory.stats.memory = {
        used: memory_used,
        // Other memory stats here?
    };

    Memory.stats.market = {
        credits: Game.market.credits,
        num_orders: Game.market.orders ? Object.keys(Game.market.orders).length : 0,
    };

    Memory.stats.cpu = Game.cpu;
    Memory.stats.cpu.used = Game.cpu.getUsed();

    //Memory.stats.roomSummary = resources.summarize_rooms();

    // Add callback functions which we can call to add additional
    // statistics to here, and have a way to register them.
    // 1. Merge in the current repair ratchets into the room summary
    // TODO: Merge in the current creep desired numbers into the room summary
    //global.stats_callbacks.fire(Memory.stats);
} // collect_stats

module.exports = {
    collect_stats,
    //add_stats_callback,
};
