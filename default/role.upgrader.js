var base = require('role.worker');

module.exports = {
    run: function(main, creep) {
        base.throe(main, creep);

        var upgradeLocation = main.asRoomPosition(creep.memory.upgradeLocation);

        if (creep.pos.isEqualTo(upgradeLocation)) {
            base.goAndDo(creep, 'upgrade', creep.room.controller,
                (t) => creep.upgradeController(t), [ERR_NOT_ENOUGH_ENERGY]);
            var container = Game.getObjectById(creep.memory.container);
            if (container && container.energy)
                base.goAndDo(creep, 'withdraw', container,
                    (t) => creep.withdraw(t, RESOURCE_ENERGY))
            //else main.log("No energy", creep);
        }
        else creep.pathify(upgradeLocation);
    }
}
