var labManager = require('labManager');

module.exports = {
    run: function(main, hive) {
        var room = hive.room; if (!room) return;
        var terminal = room.terminal; if (!terminal) return;
        if (hive.lastTickTransfer) {
            console.log("transfers."+hive.lastTickTransfer+"."+hive.name+" = "+s(labManager.transfers[hive.lastTickTransfer] ? labManager.transfers[hive.lastTickTransfer][hive.name] : "undefined"));
            delete hive.lastTickTransfer;
        }
        _.forEach(terminal.store, (amt, res) => {
            if (!amt) return;
            var transfers = labManager.transfers[res];
            if (transfers && transfers[hive.name])
                _.forEach(transfers[hive.name], (need, to) => {
                    var targetRoom = main.hives[to].room;
                    var targetTerminal = targetRoom && targetRoom.terminal;
                    if (targetTerminal) {
                        var transferAmount = Math.min(need, amt, 250000 - _.sum(targetRoom.terminal.store));
                        if (transferAmount >= (res == RESOURCE_ENERGY ? 10000 : 100)) {
                            var result = terminal.send(res, transferAmount, targetRoom.name);
                            main.log("terminal sending "+transferAmount+" "+res+" to "+to, hive);
                            console.log("transfers."+res+"."+hive.name+" = "+s(transfers[hive.name]));
                            hive.lastTickTransfer = res;
                        }
                    }
                });
        });
    }
};
