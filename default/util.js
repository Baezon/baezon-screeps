function decorate(t) {
    return typeof t === 'object' ? new Proxy(t, {
        get: function(target, prop, rcv) {
            switch (prop) {
                case 'toString': return () => s(target);
                default: return decorate(target[prop]);
            }
        },
    }) : t;
}

function creep(name) {
    var creep = Game.creeps[name];
    if (!creep) return;
    return new Proxy(creep, {
        get: function(target, prop, rcv) {
            var main = require('main');
            switch (prop) {
                case 'm': return decorate(target.memory);
                case 'Job': return main.job(target.name);
                case 'job': return decorate(target.memory.job);
                default: return decorate(target[prop]);
            }
        }
    });
}

$ = new Proxy({}, {
    get: function(target, prop, rcv) {
        var main = require('main');
        switch (prop) {
            case 'm': return decorate(Memory);
            case 'lm': return decorate(main.labManager);
            default: return decorate(Game.rooms[prop]) || creep(prop) || decorate(main.hives[prop]);
        }
        return decorate(Game.rooms[prop]) || creep(prop) || decorate(main.hives[prop]);
    }
});
