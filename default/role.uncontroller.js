
module.exports = {
    run: function(main, creep) {
        var room = Game.rooms[creep.memory.room];
        if (room) {
            var result = creep.claimController(room.controller) : creep.reserveController(room.controller);
            if (result == ERR_NOT_IN_RANGE) creep.moveTo(room.controller);
            else if (result == ERR_NO_BODYPART) creep.suicide();
            else if (result != OK) console.log(creep.name+" reservation failed: "+main.decodeErr(result));
        } else {
            creep.moveTo(creep.pos.findClosestByPath(creep.room.findExitTo(creep.memory.room)));
        }
    }
};
