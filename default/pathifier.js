/*function pathFinderOpts(plainCost, swampCost, ignoreCreeps, creepPos) {
    let opts = {
        // We need to set the defaults costs higher so that we
        // can set the road cost lower in `roomCallback`
        plainCost,
        swampCost,

        roomCallback: function(roomName) {

            let room = Game.rooms[roomName];
            // In this example `room` will always exist, but since PathFinder
            // supports searches which span multiple rooms you should be careful!
            if (!room) return;
            let costs = new PathFinder.CostMatrix;

            room.find(FIND_STRUCTURES).forEach((structure) => {
                if (structure.structureType === STRUCTURE_ROAD) {
                    costs.set(structure.pos.x, structure.pos.y, 1);
                } else if (structure.structureType !== STRUCTURE_CONTAINER &&
                          (structure.structureType !== STRUCTURE_RAMPART ||
                           !structure.my)) {
                    costs.set(structure.pos.x, structure.pos.y, 0xff);
                }
            });

            room.find(FIND_MY_CONSTRUCTION_SITES).forEach((site) => {
                if (OBSTACLE_OBJECT_TYPES.includes(site))
                    costs.set(structure.pos.x, structure.pos.y, 0xff);
            });

            // Avoid creeps in the room
            function set(x,y) {
                costs.set(x, y, ignoreCreeps ?
                    2*{plain: plainCost, swamp: swampCost}[Game.map.getTerrainAt(x, y, roomName)] : 0xff);
            }
            room.find(FIND_CREEPS).forEach((creep) => {
                let pos = creepPos && creepPos[creep.id];
                if (pos) {
                    if (pos.roomName == roomName) set(pos.x, pos.y);
                }
                else set(creep.pos.x, creep.pos.y);
            });


            return costs;
        },
    };
    return (start, goal) => {
        let goals = _.map(Array.isArray(goal) ? goal : [goal], function(goal) {
            if (goal.x !== undefined && goal.y !== undefined && goal.roomName !== undefined) {
                return {
                    range: 0,
                    pos: goal,
                };
            } else {
                if (!goal.pos) console.log(JSON.stringify(goal) + " " + new Error().stack);
                let range = Math.max(0, goal.range | 0);
                return {
                    range: range,
                    pos: goal.pos,
                };
            }
        });
        var result = PathFinder.search(start, goals, opts);
        function quality(pos) {
            return _.min(goals.map(g => g.pos.getRangeTo(pos) - g.range));
        }
        if (result.path.length && quality(result.path[result.path.length-1])+1 > quality(start))
            result.path = [];
        return result;
    }
}*/

var costMatrixCache = {};
function getCostMatrix(roomName, force) {
    if (costMatrixCache[roomName] && !force)
        return costMatrixCache[roomName];

    var room = Game.rooms[roomName];
    if (!room) return;
    var costs = new PathFinder.CostMatrix;

    room.find(FIND_STRUCTURES).forEach((structure) => {
        if (structure.structureType == STRUCTURE_ROAD) {
            costs.set(structure.pos.x, structure.pos.y, 1);
        } else if (structure.structureType != STRUCTURE_CONTAINER &&
                  (structure.structureType != STRUCTURE_RAMPART ||
                   !structure.my)) {
            costs.set(structure.pos.x, structure.pos.y, 0xff);
        }
    });

    room.find(FIND_MY_CONSTRUCTION_SITES).forEach((site) => {
        if (OBSTACLE_OBJECT_TYPES.includes(site.structureType))
            costs.set(site.pos.x, site.pos.y, 0xff);
    });
    return costMatrixCache[roomName] = costs;
}

function clearCostMatrix(roomName) {
    delete costMatrixCache[roomName];
}

var offsetsByDirection = {
    [0]: [0,0],
    [TOP]: [0,-1],
    [TOP_RIGHT]: [1,-1],
    [RIGHT]: [1,0],
    [BOTTOM_RIGHT]: [1,1],
    [BOTTOM]: [0,1],
    [BOTTOM_LEFT]: [-1,1],
    [LEFT]: [-1,0],
    [TOP_LEFT]: [-1,-1],
};

function getRoomNameFromXY(x,y) {
    return ""+(x < 0 ? 'W'+(-x-1) : 'E'+(x))+(y < 0 ? 'N'+(-y-1) : 'S'+(y));
}

function roomNameToXY(name) {
    name = name.toUpperCase();
    var match = name.match(/^(\w)(\d+)(\w)(\d+)$/);
    if(!match) return [undefined, undefined];
    var [,hor,x,ver,y] = match;
    return [hor == 'W' ? -x-1 : +x, ver == 'N' ? -y-1 : +y];
}

RoomPosition.prototype.addDirection = function(direction) {
    var [dx, dy] = offsetsByDirection[direction];
    return new RoomPosition(this.x + dx, this.y + dy, this.roomName);
};

RoomPosition.prototype.crossExit = function() {
    if (this.x == 0) {
        var [rx, ry] = roomNameToXY(this.roomName);
        return new RoomPosition(49, this.y, getRoomNameFromXY(rx-1,ry));
    }
    if (this.x == 49) {
        var [rx, ry] = roomNameToXY(this.roomName);
        return new RoomPosition(0, this.y, getRoomNameFromXY(rx+1,ry));
    }
    if (this.y == 0) {
        var [rx, ry] = roomNameToXY(this.roomName);
        return new RoomPosition(this.x, 49, getRoomNameFromXY(rx,ry-1));
    }
    if (this.y == 49) {
        var [rx, ry] = roomNameToXY(this.roomName);
        return new RoomPosition(this.x, 0, getRoomNameFromXY(rx,ry+1));
    }
    return this;
};

function pathfind(start, goal, range = 0, useNearbyCreeps, opts = {}) {
    var plainCost = opts.plainCost || opts.type == 'mil' ? 1 : 2;
    var swampCost = opts.swampCost || 5*plainCost;
    var creepPos = opts.creepPos;
    var allowedRooms;
    if (Game.map.getRoomLinearDistance(start.roomName, goal.roomName) >= 3) {
        // Use `findRoute` to calculate a high-level plan for this path,
        // prioritizing highways and owned rooms
        allowedRooms = {[start.roomName]: true};
        Game.map.findRoute(start.roomName, goal.roomName, {
        	routeCallback(roomName) {
        		var [x,y] = roomNameToXY(roomName);
        		return !(Math.abs(x) % 10) || !(Math.abs(y) % 10) ||
                    main.hiveParents[roomName] ? 1 : 2.5;
        	}
        }).forEach((info) => allowedRooms[info.room] = true);
    }
    var result = PathFinder.search(start, {pos: goal, range}, {
        plainCost, swampCost, maxOps: opts.maxOps,
        roomCallback: function(roomName) {
            if (allowedRooms && !(roomName in allowedRooms)) return false;
            var costs = getCostMatrix(roomName);
            if (useNearbyCreeps && roomName == start.roomName) {
                costs = costs ? costs.clone() : new PathFinder.CostMatrix;
                function set(x,y) {
                    costs.set(x, y, opts.ignoreCreeps ?
                        2*{plain: plainCost, swamp: swampCost}[Game.map.getTerrainAt(x, y, roomName)] : 0xff);
                }
                start.findInRange(FIND_CREEPS, 3).forEach((creep) => {
                    let pos = creepPos && creepPos[creep.id];
                    if (pos) {
                        if (pos.roomName == roomName) set(pos.x, pos.y);
                    }
                    else set(creep.pos.x, creep.pos.y);
                });
            }
            return costs;
        },
    });
    if (result.path.length && goal.getRangeTo(result.path[result.path.length-1])+1 > goal.getRangeTo(start))
        result.path = [];
    return result;
}

Creep.prototype.pathify = function(goal, range = 0, opts) {
    //if (this.name == 'Bailey') console.log(this.name+" pathify "+goal+" "+range);
    if (goal instanceof Room) goal = goal.name;
    if (typeof goal == 'string') {
        var room = Game.rooms[goal];
        var target = room && (room.storage || room.controller);
        if (target) {
            goal = target.pos;
            range = 1;
        } else {
            goal = new RoomPosition(25, 25, goal);
            range = 24;
        }
    }
    else if (goal.pos) goal = goal.pos;
    if (!(goal instanceof RoomPosition)) goal = main.asRoomPosition(goal);
    if (this.pos.getRangeTo(goal) <= range) {
        delete this.memory.path;
        return OK;
    }
    var pathObj = this.memory.path;
    if (pathObj) pathObj.dest.constructor = RoomPosition;
    if (pathObj && goal.isEqualTo(pathObj.dest = main.asRoomPosition(pathObj.dest))) {
        var onTrack = this.pos.isEqualTo(pathObj.nextPos = main.asRoomPosition(pathObj.nextPos));
        pathObj.path = onTrack ? pathObj.path.slice(1) : pathObj.path;
        //if (this.name == 'Bailey') console.log(this.name+" 1 onTrack"+onTrack+" pathObj="+s(pathObj));
        if (pathObj.path.length) {
            var direction = +pathObj.path[0];
            var newPos = this.pos.addDirection(direction).crossExit();
            //if (this.name == 'Bailey') console.log(this.name+" 2 onTrack"+onTrack+"newPos="+newPos+" pathObj="+s(pathObj));
            if (onTrack || newPos.isEqualTo(pathObj.nextPos) &&
                (pathObj.timeout || (pathObj.timeout = Game.time)) >= Game.time) {
                var result = direction ? this.move(direction) : OK;
                //if(this.name == 'Bailey') console.log(result);
                if (result == OK) pathObj.nextPos = newPos;
                else pathObj.path = '0' + pathObj.path;
                if (onTrack) delete pathObj.timeout;
                return result;
            }
            //else if (pathObj.timeout < Game.time && this.name == 'Bailey') console.log(this.name+" repathing because timeout");
            //else if (this.name == 'Bailey') console.log(this.name+" repathing because off track: "+this.pos+" "+pathObj.nextPos+" "+newPos);
        }  //else if (this.name == 'Dirac')console.log(this.name+" repathing because no path" );
    }// else console.log(this.name+" repathing because new dest "+(pathObj && pathObj.dest)+" => "+goal);
    var useNearbyCreeps = pathObj && pathObj.timeout < Game.time;
    delete this.memory.path;

    var pathfObj = pathfind(this.pos, goal, range, useNearbyCreeps, opts);
    var path = pathfObj.path;
    if (!path.length) return OK;
    if (pathfObj.incomplete) path.length = (path.length+1)/2|0;
    var lastPos = this.pos;
    var result = '';
    path.forEach(pos => {
        if (pos.roomName == lastPos.roomName)
            result += lastPos.getDirectionTo(pos);
        lastPos = pos;
    });
    if (path[0].roomName != this.pos.roomName) result = '0' + result;
    if (Game.map.getTerrainAt(path[0]) == 'wall') return ERR_NO_PATH;
    this.memory.path = {dest: goal, nextPos: path[0], path: result};
    var direction = +result[0];
    return direction ? this.move(direction) : OK;
};

module.exports = {
    /*squadLeader: pathFinderOpts(1, 5, false),
    squadMuster: pathFinderOpts(1, 5, false),
    squadMove: pathFinderOpts(1, 5, true),
    squadAttack: (creepPos) => pathFinderOpts(1, 5, false, creepPos),
    depositDist: pathFinderOpts(2, 10, true),
    roadBuild: pathFinderOpts(1, 2, true),
    civ: pathFinderOpts(2, 10, false),*/
    getCostMatrix,
    clearCostMatrix,
    offsetsByDirection,
    pathfind,
};
