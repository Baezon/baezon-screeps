/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('tower');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function(main, hive, towers) {
        var room = hive.room;
        if (!room) return;
        if (!towers.length) return;

        var targets = main.filterNonAllies(room.find(FIND_HOSTILE_CREEPS, {filter: (creep) =>
            creep.body.some((part) => [ATTACK, RANGED_ATTACK, WORK, HEAL].includes(part.type))}));
        var lostHealing = {};
        var baseline = 0;
        targets.forEach((creep) => lostHealing[creep.id] = 0);
        var friendlies = room.find(FIND_MY_CREEPS, {filter: (creep) =>
            creep.body.some((part) => [ATTACK, RANGED_ATTACK].includes(part.type))});

        targets.forEach((healer) => {
            var healPower = 0;
            healer.body.forEach((part) => {
                if(part.type == HEAL && part.hits > 0)
                    healPower += main.partEffectiveness(part);
            });
            if (healPower > 0) {
                baseline += healPower*12;
                targets.forEach((creep) => {
                    var dist = creep.pos.getRangeTo(healer.pos);
                    lostHealing[creep.id] += healPower *
                        (dist <= 1 ? 0 : dist <= 3 ? 8*(dist-1) : 12*dist-20);
                });
            }
        });
        /*targets.forEach((creep) => {
            creep.body.some((part) => {
                if (part.hits == 0) return;
                if(part.type == TOUGH)
                    lostHealing[creep.id] *= main.partEffectiveness(part);
                return true;
            })
        });*/
        friendlies.forEach((friendly) => {
            var atk = 0, rangedAtk = 0;
            friendly.body.forEach((part) => {
                if(part.hits > 0) {
                    if (part.type == ATTACK)
                        atk += main.partEffectiveness(part);
                    else if (part.type == RANGED_ATTACK)
                        rangedAtk += main.partEffectiveness(part);
                }
            });
            if (atk > 0 || rangedAtk > 0) {
                baseline -= atk*30 + rangedAtk*10;
                targets.forEach((creep) => {
                    var dist = creep.pos.getRangeTo(friendly.pos);
                    lostHealing[creep.id] -= atk * 30*(dist-1) + rangedAtk *
                        (dist <= 1 ? 0 : dist <= 2 ? 7 : dist <= 3 ? 16 : 10*dist-14);
                });
            }
        });
        var best = _.max(_.values(lostHealing));
        var goodTargets = best ? _.filter(targets, (creep) => lostHealing[creep.id] == best) : room.find(FIND_HOSTILE_CREEPS);
        var baseDamage;
        function towerDamage(creep) {
            var damage = 0;
            towers.forEach((tower) => {
                var dist = tower.pos.getRangeTo(creep.pos);
                damage += dist <= 5 ? 600 : dist <= 20 ? 30*(25-dist) : 150;
            });
            baseDamage = damage;
            creep.body.some((part) => {
                if (part.hits == 0) return;
                damage *= main.partEffectiveness(part);
                return true;
            });
            return damage;
        };
        var target = goodTargets.length && _.max(goodTargets, towerDamage);
        if (target && towerDamage(target) < baseline) {
            if (target.hits > best || _.sum(target.body, (part) => part.hits *
                (part.type == TOUGH ? 1/main.partEffectiveness(part) : 1)) > best) target = undefined;
        }
        if (target) {
            towers.forEach((tower) => tower.attack(target));
        }
        else if (hive.alertMode) {
            target = _.min(room.find(FIND_STRUCTURES, {filter: (s) =>
                (s.structureType == STRUCTURE_RAMPART || s.structureType == STRUCTURE_WALL)}), s => s.hits);
            if (target && target.hits < hive.wallStrength) {
                towers.forEach((t) => t.repair(target));
            }
        } else {
            repairTargets = room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_RAMPART && s.hits < 5000 && !s.pos.lookFor(LOOK_FLAGS).length});
            repairTargets.sort((a,b) => a.hits - b.hits);
            if (repairTargets.length) {
                for(i = 0; i < towers.length; i++){
                    if(!repairTargets[i]) break;
                    towers[i].repair(repairTargets[i]);
                }
            } else {
                healTargets = room.find(FIND_MY_CREEPS, {filter: (c) => c.hits != c.hitsMax});
                healTargets.sort((a,b) => a.hits - b.hits);
                if (healTargets.length) {
                    for(i = 0; i < towers.length; i++){
                        if(!healTargets[i]) break;
                        towers[i].heal(healTargets[i]);
                    }
                }
            }
        }
    }
};
