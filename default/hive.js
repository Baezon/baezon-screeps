var pathifier = require('pathifier');
var labManager = require('labManager');
var name = require('names')

function Hive(hive, obj) {
    this.roomName = hive;
    if(obj) Object.assign(this, obj);
}
Hive.prototype = {
    domain: [],
    highways: [],
    uncontrols: [],
    numWorkers: 5,
    numCarriers: 0,
    wallStrength: 10000,
    upgradeContainers: [],
    upgradeLocations: [],
    resourceMax: {
        energy: 400000,
        G: 10000,
        UH2O: 10000,
        KHO2: 10000,
        LHO2: 10000,
        ZH2O: 10000,
        ZHO2: 10000,
        GHO2: 10000,
    },

    init: function(hiveParents, parent) {
        hiveParents[this.roomName] = this.name = parent;
        this.domain.forEach((name) => hiveParents[name] = parent);
        this.highways.forEach((name) => hiveParents[name] = parent);
        this.uncontrols.forEach((name) => hiveParents[name] = parent);
        var room = this.room;
        if (!room) return;
        this.upgradeLocations = [];
        this.upgradeContainers = room.controller.pos.findInRange(FIND_STRUCTURES, 1)
            .filter((s) => s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_LINK).map((container) => {
            var pos = container.pos;
            for (var x = pos.x-1; x <= pos.x + 1; x++) for (var y = pos.y-1; y <= pos.y + 1; y++) {
                var pos2 = container.room.getPositionAt(x, y);
                if (pos2 && !pos2.look().some((vague) =>
                  (vague.type == 'terrain' && vague.terrain == 'wall') ||
                    vague.type == 'structure' &&
                    vague.structureType != STRUCTURE_CONTAINER &&
                    vague.structureType != STRUCTURE_RAMPART))
                    this.upgradeLocations.push(pos2);
            }
            return container.id;
        });
        if(!this.guards) this.guards = [];
        this.domain.forEach((name) => {
            if (/^[WE][0-9]*([46][NS][0-9]*[4-6]|5[NS][0-9]*[46])$/.test(name))
                this.guards.push({leadTime: 150 + Game.map.findRoute(thisroomName, name).length * 50,
                    color: COLOR_YELLOW, layout: ['patroller']});
        });

        var nuker = this.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_NUKER})[0];
        this.nukerID = nuker ? nuker.id : undefined;

        this.initLabs();
        this.clearBulletin();
        /*Object.defineProperty(this, 'resourceGroup', {
            get: () => { return this._resourceGroup; },
            set: (value) => {
                if (value != this._resourceGroup) console.log("main.hives."+this.name+".resourceGroup: " + this._resourceGroup + " => "+value);
                return this._resourceGroup = value;
            },
        });*/
    },
    initLabs: function() {
        if (this.labType) {
            this.resourceMax = _.clone(this.resourceMax);
            ['H','O','U','K','L','Z','X'].forEach(res => this.resourceMax[res] = 10000);
        }
        var room = this.room;
        if (!room) return;
        var labs = room.find(FIND_STRUCTURES, {filter: {structureType: STRUCTURE_LAB}});
        var outOfRange = labs.map(lab => labs.filter(lab2 => lab.pos.getRangeTo(lab2) > 2));
        var numOOR = outOfRange.some(a => a.length).length;
        if (numOOR > 0 && numOOR <= 4) {
            this.labs = [];
            var arr = [];
            labs.forEach((lab, i) => {
                if (outOfRange[i].length == 2)
                    arr.push([lab, outOfRange[i]]);
            });
            if (arr[0]) [this.labs[8], [this.labs[0], this.labs[1]]] = arr[0];
            if (arr[1]) [this.labs[9], [this.labs[2], this.labs[3]]] = arr[1];
            labs = labs.filter(lab => !this.labs.includes(lab));
            for (var i = 0; labs.length; i++)
                if (!this.labs[i]) this.labs[i] = labs.pop();
        } else {
            this.labs = _.sortBy(labs, (lab, i) => outOfRange[i].length);
        }
    },
    toString: function() {
        return "Hive " + this.name;
    },
    get room() { return Game.rooms[this.roomName]; },
    get center() {
        let room = this.room; if (!room) return;
        return room.storage || room.find(FIND_STRUCTURES,
            {filter: (structure) => structure.structureType == STRUCTURE_SPAWN})[0] || room.controller;
    },
    get hiveAndDomain() { return [this.roomName].concat(this.domain); },

    get deposits() {
        let out = [];
        var allGood = true;
        this.hiveAndDomain.forEach((room) => {
            var rc = require('main').roomCache(room);
            if (rc) _.forEach(rc.deposits, (deposit, id) => out.push({id, room}));
            else allGood = false;
        });
        if (allGood) this.deposits = () => out;
        return out;
    },

    _controllerStatus: {},
    lastControllerStatus: function(roomName) {
        var room = Game.rooms[roomName];
        return room ? (this._controllerStatus[roomName] = room.controller ? {
            owner: room.controller.owner,
            reservation: room.controller.reservation,
            my: room.controller.my,
        } : {none: true}) : this._controllerStatus[roomName];
    },

    _distance: {},
    distanceTo: function(hive) {
        return this._distance[hive.name] || (this._distance[hive.name] =
            Game.map.findRoute(this.room, hive.room).length);
    },

    get outbox() {
        if(!Memory.hives) Memory.hives = {};
        var obj = Memory.hives[this.name];
        if(!obj) Memory.hives[this.name] = obj = {};
        return obj.outbox || (obj.outbox = []);
    },
    get outboxRes() {
        if(!Memory.hives) Memory.hives = {};
        var obj = Memory.hives[this.name];
        if(!obj) Memory.hives[this.name] = obj = {};
        return obj.outboxRes || (obj.outboxRes = {});
    },

    send: function(resourceType, amount, destination, description) {
        if(!this.room.terminal) return this.name + " I don't have a terminal!", this;
        this.outbox.push({gift: true, res: resourceType, amt: amount, dest: destination, description});
        labManager.addTo(this.outboxRes, resourceType, amount);
    },

    deal: function(orderId, amount) {
        if(!this.room.terminal) return this.name + " I don't have a terminal!";
        var order = Game.market.getOrderById(orderId);
        if(!order) return this.name + " Order doesn't exist";
        if (order.type == ORDER_BUY) {
            this.outbox.push({gift: false, res: order.resourceType, amt: amount, id: orderId});
            labManager.addTo(this.outboxRes, resourceType, amount);
        } else return main.decodeErr(Game.market.deal(orderId, amount, this.roomName));
    },


    isOptimal: function(struct) {
        switch (struct.structureType) {
            case STRUCTURE_ROAD: return struct.hits >= .8*struct.hitsMax;
            case STRUCTURE_WALL: return true;
            case STRUCTURE_RAMPART: return struct.hits >= Math.min(this.wallStrength + 100000, this.wallStrength*2);
            case STRUCTURE_CONTROLLER: return true;
            default: return struct.hits >= struct.hitsMax;
        }
    },

    isCritical: function(struct) {
        switch (struct.structureType) {
            case STRUCTURE_ROAD: return struct.hits < .2*struct.hitsMax;
            case STRUCTURE_WALL: return false;
            case STRUCTURE_RAMPART: return struct.hits < this.wallStrength;
            case STRUCTURE_CONTAINER: return struct.hits <= .5*struct.hitsMax;
            case STRUCTURE_CONTROLLER: return false;
            default: return struct.hits < struct.hitsMax;
        }
    },

    clearBulletin: function() {
        var room = this.room;

        var store = room && room.storage ? _.clone(room.storage.store) : {};
        var addStore = store2 => _.forEach(store2, (amt, res) => labManager.addTo(store, res, amt));
        if (room && room.terminal) addStore(room.terminal.store);
        this.bulletin = {
            roles: {carrier: [], miner: [], upgrader: [], worker: [],
                reserver: [], custom: [], defender: [],},
            jobs: {harvester: [], builder: [], repairer: [], dismantler: [],
                extensionFilling: [], transfer: [], upgrader: [], donate: [],
                emergencyRepairer: [], labTending: [], nukerFilling: [],
                wallStrengthener: [],},
            //workersEmployed: 0,
            numCreeps: 0,
            reservers: {},
            miners: {},
            harvesters: {},
            upgraderClaims: {},
            retrieval: {},
            store,
            addStore,
            populate: function(creep, reemploy) {
                addStore(creep.carry);
                if (require('main').MILITARY_ROLES.includes(creep.memory.role)) return;
                if(!creep.memory.role) return;
                var elderly = creep.ticksToLive <= 3*creep.body.length;
                if (!reemploy && !elderly) {
                    this.numCreeps++;
                    this.roles[creep.memory.role].push(creep.id);
                }
                switch (creep.memory.role) {
                case 'reserver':
                    if (!elderly) this.reservers[creep.memory.room] = creep.id;
                    break;

                case 'miner':
                    if (!elderly) this.miners[creep.memory.deposit.id] = creep.id;
                    break;

                case 'upgrader':
                    if (!elderly) {
                        var loc = creep.memory.upgradeLocation;
                        this.upgraderClaims[loc.x+","+loc.y] = creep.id;
                    }
                    break;

                case 'worker':
                case 'carrier':
                    var job = creep.memory.job;
                    if (job) {
                        //if (creep.memory.role == 'worker') this.workersEmployed += 1;
                        if(!job.type) {
                            delete creep.memory.job;
                            return;
                        }
                        this.jobs[job.type].push(creep.id);
                        if (job.type == 'harvester' && job.deposit) {
                            this.harvesters[job.deposit.id] = (this.harvesters[job.deposit.id] || 0) + 1;
                        } else if (job.type == 'transfer') {
                            switch (job.subType) {
                            case 'minerRetrieval':
                            case 'garbageCollection':
                            case 'shipping':
                            case 'terminalEmptying':
                            case 'terminalFilling':
                                this.retrieval[job.withdrawTarget] = (this.retrieval[job.withdrawTarget] || 0) + 1;
                                break;
                            case 'labTending':
                                this.jobs.labTending.push(creep.id);
                                break;
                            case 'nukerFilling':
                                this.jobs.nukerFilling.push(creep.id);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        };
    },

    get hasEnergy() {
        var room = this.room;
        return room && (room.storage ? room.storage.store[RESOURCE_ENERGY] > 10000
            : room.energyAvailable >= .8*room.energyCapacityAvailable && room.energyAvailable > 0);
    },

    buildCreep: function(main, spawn) {
        if (spawn.spawning) return;
        var roles = this.bulletin.roles;
        var room = this.room;
        if(!room) return;


        var buildInner = (body, mem) => {
            if (room.energyAvailable < main.creepBodyCost(body)) return ERR_NOT_ENOUGH_ENERGY;
            mem.hive = this.name;
            var result = spawn.createCreep(body, name.gen() , mem);
            if (result < 0) {
                if (result == ERR_BUSY) ;
                else console.log("Can't create "+mem.role+": " + Game.main.decodeErr(result), 1);
            }
            else main.log("Creating "+mem.role+" "+result, this);
            return result;
        };

        var roleCheck = {
            carrier: {
                code: () => {
                    return buildInner(main.creepBody([], [CARRY, CARRY, MOVE], [],
                        roles.carrier.length ? .8*room.energyCapacityAvailable : room.energyAvailable),
                        {role: 'carrier'});
                },
                number: roles.carrier.length,
                max: room.storage ? ((_.sum(room.storage.store) > 900000 && room.storage.energy > 200000) ? 3 : /*_.sum(this.deposits, deposit => +!!deposit.container)*/ roles.miner.length + this.numCarriers || 1) : 0,
            },
            miner: {
                code: () => {
                    var result;
                    this.deposits.some(deposit => {
                        if (main.getDeposit(deposit).container &&
                            !this.bulletin.miners[deposit.id]) {
                            var target = Game.getObjectById(deposit.id);
                            if (target) {
                                if (target.structureType == STRUCTURE_EXTRACTOR) {
                                    var mineral = target.pos.lookFor(LOOK_MINERALS)[0];
                                    if (mineral.mineralAmount > 0 && !room.storage.store[mineral.mineralType] > 500000) {
                                        result = buildInner([WORK,WORK,MOVE], {role: 'miner', deposit});
                                    }
                                } else {
                                    result = buildInner(main.creepBody([], [WORK,WORK,MOVE], [WORK,MOVE], .8*room.energyCapacityAvailable, 2),
                                        {role: 'miner', deposit});
                                }
                                if (result) {
                                    this.bulletin.miners[deposit.id] = '?';
                                    return result;
                                }
                            }
                        }
                    });
                    return result;
                },
                number: roles.miner.length,
                max: (room.storage && _.sum(room.storage.store) > 900000 && room.storage.energy > 200000) ? 0 : this.deposits.filter((deposit) => main.getDeposit(deposit).container).length,
            },
            worker: {
                code: () => {
                    return buildInner(main.creepBody([], [WORK, CARRY, MOVE], [],
                        roles.worker.length ? Math.min(.8*room.energyCapacityAvailable,2400) : room.energyAvailable), {role: 'worker'});
                },
                number: roles.worker.length,
                max: this.numWorkers,
            },
            upgrader: {
                code: () => {
                    var target;
                    this.upgradeLocations.some((pos) => {
                        if (this.bulletin.upgraderClaims[pos.x+","+pos.y]) return false;
                        target = pos;
                        return true;
                    });
                    if (target) {
                        return buildInner(main.creepBody([], [WORK,WORK,WORK,WORK,WORK,MOVE], [CARRY],
                             .8*room.energyCapacityAvailable, this.room.controller.level == 8 ? 3 : undefined), {
                            role: 'upgrader',
                            container: this.upgradeContainers[0],
                            upgradeLocation: target
                        });
                    }
                },
                number: roles.upgrader.length,
                max: room.storage ? (room.controller.level == 8 ? (room.storage.store[RESOURCE_ENERGY] > 100000 ? 1 : 0) : (room.storage.store[RESOURCE_ENERGY])/100000|0) : 0,
            },
            reserver: {
                code: () => {
                    if (this.hasEnergy) {
                        var result;
                        this.domain.some((name) => {
                            var controllerStatus = this.lastControllerStatus(name);
                            if (!this.bulletin.reservers[name]) {
                                if (!controllerStatus || (!controllerStatus.none && !controllerStatus.owner)) {
                                    return result = buildInner([CLAIM,CLAIM,MOVE,MOVE], {role: 'reserver', room: name});
                                }
                            }
                        })
                        if(result) return result;
                        this.uncontrols.some((name) => {
                            var controllerStatus = this.lastControllerStatus(name);
                            if (!this.bulletin.reservers[name]) {
                                if (!controllerStatus || (!controllerStatus.none && controllerStatus.owner && !controllerStatus.my)) {
                                    main.log("building reserver for "+name, this);
                                    return result = buildInner([CLAIM,CLAIM,CLAIM,CLAIM,CLAIM,MOVE,MOVE,MOVE,MOVE,MOVE], {role: 'reserver', room: name});
                                }
                            }
                        })
                    }
                },
                number: roles.reserver.length,
                max: (this.room.energyCapacityAvailable >= 1100 ? this.domain.length + (this.room.energyCapacityAvailable >= 3250 ? this.uncontrols.length : 0) : 0),
            },
        };
        var priority = (roleObj) => roleObj.max <= 0 ? Infinity : roleObj.number / roleObj.max;

        //SQUAD CREATION
        var result;
        if(priority(roleCheck.carrier) >= .5 || priority(roleCheck.worker) >= .5) {
            main.squads.some((squad) => {
                if (squad.hive == this.name) {
                    var out = squad.build(main, squad.budget >= 1 ?
                        Math.min(squad.budget, room.energyCapacityAvailable) :
                        (squad.budget || 1)*room.energyCapacityAvailable);

                    //console.log("get "+JSON.stringify(out));
                    if (out) {
                        var [i, body] = out;
                        //console.log("want "+JSON.stringify(body));
                        result = buildInner(body, {role: squad.layout[i]});
                        if (result) {
                            if (!(result < 0)) squad.receive(i, result);
                            else main.log("trying "+main.decodeErr(result), this);
                            return result;
                        }
                    }
                }
            });
            if(result) return result;
        }

        //DEFENDER CREATION
        if(this.alertMode && this.alertMode/30 > roles.defender.length) {
            result = buildInner(main.creepBody([MOVE], [RANGED_ATTACK,RANGED_ATTACK], [], undefined), {role: 'defender', targetRoom: this.room.name});
        }
        if(result) return result;

        var roleArr = Object.keys(roleCheck);
        roleArr = _.sortBy(roleArr, (a) => priority(roleCheck[a]));

        //main.log(roleArr, this);
        //V Bigger = more need for that creep V
        //main.log(roleArr.map((a) => priority(roleCheck[a])), this);
        var result;
        roleArr.some((role) => priority(roleCheck[role]) < 1 && (result = roleCheck[role].code()));
        return result;
    },


    assignJob: function(creep) {
        switch (creep.memory.role) {
            case 'worker':
                this.assignJobWorker(creep);
                this.bulletin.populate(creep, true);
                break;
            case 'carrier':
                this.assignJobCarrier(creep);
                this.bulletin.populate(creep, true);
                break;
        }
    },

    assignJobWorker: function(creep) {
        function setJob(type, pos) { return creep.memory.job = {type, pos}; }
        var hasEnergy = this.hasEnergy;
        var storage = this.room.storage;
        var jobs = this.bulletin.jobs;

        if (_.some(creep.carry, (val, res) => res != RESOURCE_ENERGY)) {
            this.assignJobCarrier(creep);
            if (creep.memory.job) return;
        }

        if (this.room.controller.ticksToDowngrade <= 4000 && hasEnergy) {
            setJob('upgrader');
            return;
        }

        if (this.alertMode) {
            var weakestWall = _.min(this.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_WALL || s.structureType == STRUCTURE_RAMPART}), (s) => s.hits);
            if (weakestWall) {
                setJob('emergencyRepairer').pos = weakestWall.pos;
                return;
            }
        }

        if (jobs.builder.length < 2 && hasEnergy) {
            var sites = [];
            if (this.hiveAndDomain.concat(this.highways).some((name) => {
                var room = Game.rooms[name];
                if (room) {
                    var sites = room.find(FIND_CONSTRUCTION_SITES);
                    if (sites.length) {
                        return sites.some((site) => {
                            if (site.structureType != STRUCTURE_RAMPART ||
                                room.find(FIND_STRUCTURES,
                                {filter: (s) => s.structureType == STRUCTURE_TOWER &&
                                    s.energy >= s.energyCapacity*.2}).length) {
                                Object.assign(setJob('builder'), {site: site.id, pos: site.pos});
                                require('main').roomCacheAddStructure(site);
                                return true;
                            }
                        });
                    }
                }
                return false;
            })) return;
        }

        if (jobs.repairer.length < 4 && hasEnergy) {
            var roads = [];
            this.hiveAndDomain.concat(this.highways).forEach((name) => {
                var room = Game.rooms[name];
                if (room) {
                    roads = roads.concat(room.find(FIND_STRUCTURES, {filter: (structure) => !this.isOptimal(structure) &&
                        _.filter(structure.pos.lookFor(LOOK_FLAGS), (flag) => flag.color == COLOR_RED).length == 0}));
                }
            });
            var dispatch = (list) => list.some((road) => {
                if(this.isCritical(road)) {
                    list.sort((a,b) => (a.hits/a.hitsMax) - (b.hits/b.hitsMax));
                    setJob('repairer').list = list.map((road) => road.id);
                    return true;
                }
                return false;
            });
            if (dispatch(roads.filter((structure) =>
                structure.structureType != STRUCTURE_WALL && structure.structureType != STRUCTURE_RAMPART))) return;
            if (storage && storage.energy > 200000 && dispatch(roads.filter((structure) =>
                structure.structureType == STRUCTURE_WALL || structure.structureType == STRUCTURE_RAMPART))) return;
        }

        if (jobs.dismantler.length < 2) {
            if(this.hiveAndDomain.some((name) => {
                var room = Game.rooms[name];
                return room && room.find(FIND_FLAGS).some((flag) => {
                    if (flag.color == COLOR_RED) {
                        var structs = flag.pos.lookFor(LOOK_STRUCTURES);
                        if (flag.secondaryColor == COLOR_GREEN) {
                            structs = structs.filter(s => s.structureType == STRUCTURE_RAMPART);
                        } else if (flag.secondaryColor == COLOR_PURPLE) {
                            structs = structs.filter(s => s.structureType != STRUCTURE_RAMPART);
                        }
                        if(structs.length) {
                            setJob('dismantler').target = structs[0].id;
                            return true;
                        }
                    }
                });
            })) return;
        }

        var harvesters = this.bulletin.harvesters;
        var main = require('main');
        if (_.sortBy(this.deposits, deposit => harvesters[deposit.id] || 0).some((deposit) => {
            if (!deposit.id) {
                main.log("deposit: "+s(deposit), this);
                return;
            }
            if(this.bulletin.miners[deposit.id] || harvesters[deposit.id] >= main.getDeposit(deposit).capacity) return;
            var source = Game.getObjectById(deposit.id);
            if (!source || source.energy) {
                setJob('harvester').deposit = deposit;
                return true;
            }
        })) return;

        if (storage) {
            this.assignJobCarrier(creep);
            if (creep.memory.job) return;
        }

        if(storage && storage.energy >= 100000 && creep.memory.unemployed >= 10) {
            var weakestWall = _.min(this.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_RAMPART}), (s) => s.hits);
            if(weakestWall) {
                setJob('wallStrengthener').wall = weakestWall.id;
                return;
            }
        }

        if (hasEnergy && this.room.controller.level < 8) {
            setJob('upgrader');
            return;
        }

        /*console.log("random!");
        var deposit = this.deposits[Math.floor(Math.random()*this.deposits.length)];
        setJob('harvester').deposit = deposit;*/
    },

    assignJobCarrier: function(creep) {
        function setJob(type, subType, resource, withdrawTarget, transferTarget, dumpTarget) {
            return creep.memory.job = {type, subType, dumpTarget, withdrawTarget, transferTarget, resource};
        }

        var room = this.room;
        var storage = room.storage;
        if(!storage) return;

        if (!this.bulletin.jobs.extensionFilling.length && storage && storage.energy > 10000 && creep.memory.role != 'worker' && this.bulletin.roles.carrier.length != 1) {
            setJob('extensionFilling').permanent = true;
            return;
        }
        if (room.energyAvailable < .9*room.energyCapacityAvailable && this.bulletin.jobs.extensionFilling.length < 2 && storage && storage.energy > 5000) {
            setJob('extensionFilling');
            return;
        }

        var dump = _.sum(creep.carry) ? storage.id : undefined;
        var target = room.find(FIND_STRUCTURES, {filter: (s) =>
            (s.structureType == STRUCTURE_SPAWN ||
                s.structureType == STRUCTURE_EXTENSION ||
                s.structureType == STRUCTURE_TOWER ||
                s.structureType == STRUCTURE_CONTAINER)
                && s.pos.lookFor(LOOK_FLAGS)[0] && s.energy > 1
        })[0];
        if(target) {
            setJob('transfer', 'energyRemoval', RESOURCE_ENERGY, target.id, storage.id, dump);
            return;
        }

        if(creep.ticksToLive > 50) {
            if(room.terminal && _.sum(storage.store) <= 950000 && (this.bulletin.retrieval[room.terminal.id] || 0) <= 1) {
                if (_.some(room.terminal.store, (amt, res) => {
                    var obj = labManager.transfers[res];
                    obj = obj && obj[this.name];

                    var transfersOut = (obj ? _.sum(obj) : 0) + (this.outbox[res] || 0);
                    if (amt > (transfersOut ? transfersOut + creep.carryCapacity : 0) + (res == RESOURCE_ENERGY ? 120000 : 0)) {
                        main.log("terminal emptying: "+res+" amt "+amt, creep);
                        setJob('transfer', 'terminalEmptying', res, room.terminal.id, storage.id, dump);
                        return true;
                    }
                })) return;
            }

            if (room.terminal && _.sum(room.terminal.store) <= 290000) {
                var terminalFillJobs = [];
                room.find(FIND_STRUCTURES, {filter: s.structureType == STRUCTURE_STORAGE ||
                    s.structureType == STRUCTURE_LAB}).forEach(s => {
                    var terminalFill = (amt, res) => {
                        var obj = labManager.transfers[res];
                        obj = obj && obj[this.name];
                        var transfersOut = Math.max((obj ? _.sum(obj) : 0) - (room.terminal.store[res] || 0) + (this.outboxRes[res] || 0), 0);
                        if (amt > transfersOut) amt = transfersOut;
                        if (amt >= 500) terminalFillJobs.push({target: s, res, amt});
                    };
                    if (!this.bulletin.retrieval[s.id]) {
                        if (s.structureType == STRUCTURE_STORAGE)
                            _.forEach(s.store, terminalFill);
                        else if (s.mineralAmount)
                            terminalFill(s.mineralAmount, s.mineralType);
                    }
                });
                if (terminalFillJobs.length) {
                    var fillJob = _.max(terminalFillJobs, j => j.amt);
                    main.log("terminal filling from " + fillJob.target + " " + fillJob.res + " amt " + fillJob.amt, creep);
                    setJob('transfer', 'terminalFilling', fillJob.res, fillJob.target.id, room.terminal.id, dump);
                    return;
                }
            }
            if (!this.bulletin.jobs.labTending.length && labManager.calcResourceGroup(main, this) != undefined && this.labType) {
                var group = labManager.RESOURCE_GROUPS[this.labType][this.resourceGroup];
                var groupInputs = labManager.GROUP_INPUTS[this.labType][this.resourceGroup];
                //main.log("resourceGroup = "+this.resourceGroup+ " = "+group, this);
                if (group.some((res, i) => {
                    var lab = this.labs[i];
                    if (lab && lab.mineralType && (lab.mineralType == res ? lab.mineralAmount > 2500 && !groupInputs.includes(res) : lab.mineralAmount > 0)) {
                        main.log("emptying lab " + i + " at "+lab.pos + ", " + lab.mineralAmount + " " + lab.mineralType + " => " + res + " group "+group, creep);
                        setJob('transfer', 'labTending', lab.mineralType, lab.id, storage.id, dump);
                        return true;
                    }
                })) return;
                var inputs = labManager.reactionInputs[this.name];
                //if(this.name == "Alpha") console.log("inputs"+s(inputs));
                if (inputs) {
                    var inputValue = i => {
                        var res = group[i];
                        if (!groupInputs.includes(res)) return 0;
                        var lab = this.labs[i];
                        //if(this.name == "Alpha") console.log(res + " " + i + " "+(lab.mineralAmount <= 500) + " " + (storage.store[res] || 0)+","+ (inputs[res] || 0)+","+
                        //    (lab.mineralCapacity - lab.mineralAmount))
                        return lab.mineralAmount <= 500 ? Math.min(storage.store[res] || 0, inputs[res] || 0,
                            lab.mineralCapacity - lab.mineralAmount) : 0;
                    };
                    var index = _.max(_.range(group.length), inputValue), bestInput = group[index];
                    //if(this.name == "Alpha") console.log(bestInput+ " " + index + " " + inputValue(index));
                    if (inputValue(index)) {
                        var lab = this.labs[index];
                        main.log("filling lab "+index + " at "+lab.pos+" with "+bestInput+" group "+group, creep);
                        setJob('transfer', 'labTending', bestInput, storage.id, lab.id, dump);
                        return;
                    }
                }
            }
            //if(this.name == "Alpha") console.log("no labtending");

            var nuker = Game.getObjectById(this.nukerID);
            if (nuker && nuker.ghodium != nuker.ghodiumCapacity && !this.bulletin.jobs.nukerFilling.length && storage.store[RESOURCE_GHODIUM]) {
                setJob('transfer','nukerFilling', RESOURCE_GHODIUM, storage.id, this.nukerID);
                return;
            }
        }

        var factor = Math.min(5*(room.energyCapacityAvailable*.8/150|0), 100);
        var drops = [];
        this.hiveAndDomain.forEach((name) => {
            var room = Game.rooms[name];
            if(room) room.find(FIND_DROPPED_RESOURCES, {filter: (res) =>
                !this.bulletin.retrieval[res.id]}).forEach(res => drops.push(res));
        });
        this.deposits.forEach((deposit) => {
            var obj = main.getDeposit(deposit);
            if (obj.container && (this.bulletin.retrieval[obj.container] || 0)*factor < obj.distance) {
                var o = Game.getObjectById(obj.container);
                if (o) drops.push(o);
            }
        });
        function dropSize(obj) {
            return obj instanceof Resource ? (obj.resourceType == RESOURCE_ENERGY ? 1 : 5) * obj.amount : _.sum(obj.store);
        }
        var drop = drops.sort((a,b) => dropSize(b) - dropSize(a))[0];
        //if(this.name == 'Alpha') console.log(drop);
        if (drop && dropSize(drop)) {
            if (drop instanceof Resource) {
                setJob('transfer', 'garbageCollection', null, drop.id, storage.id, dump);
            } else if (drop.energy > 0) {
                setJob('transfer', 'minerRetrieval', RESOURCE_ENERGY, drop.id, storage.id, dump);
            } else {
                setJob('transfer', 'minerRetrieval', null, drop.id, room.storage.id, dump);
            }
            return;
        }
    },
};

module.exports = Hive;
