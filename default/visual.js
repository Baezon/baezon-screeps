var main = require('main');
module.exports = {
    orders: {
        drawPath: function(creepName, strong) {
            var creep = Game.creeps[creepName];
            if (!creep) return;
            var path = creep.memory.path;
            var pos = creep.pos;
            var viz = new RoomVisual(pos.roomName);
            if (strong)
                viz.circle(pos,
                    {fill: 'transparent', radius: 0.55, stroke: '#66A'});
            if (path) {
                var style = strong ? {stroke: '#66A', lineStyle: 'dashed'} :
                    {stroke: '#fff', lineStyle: 'dotted', opacity: .2};
                var posArr = [pos];
                _.forEach(path.path, direction => {
                    var newPos = pos.addDirection(direction);
                    if (viz.roomName != pos.roomName) {
                        viz.poly(posArr, style);
                        viz = new RoomVisual(pos.roomName);
                        posArr = [pos];
                    }
                    posArr.push(newPos);
                    pos = newPos.crossExit();
                });
                viz.poly(posArr, style);
            }
            return true;
        },

        drawJobInfo: function(creepName) {
            var mem = Game.creeps[creepName].memory;
            var title = mem.role;
            function textAt(text, pos, style) {
                //main.log(text + " at "+ pos, Game.creeps[creepName]);
                new RoomVisual(pos.roomName).text(text, pos.x, pos.y+(style && style.lower || .25*(style && style.size || .5)), style);
            }
            function circleAt(pos, style) {
                //main.log("circle at "+ pos, Game.creeps[creepName]);
                new RoomVisual(pos.roomName).circle(pos, style);
            }
            switch (mem.role) {
                case 'worker':
                case 'carrier':
                    var job = mem.job;
                    if (!job) return true;
                    switch (job.type) {
                    case 'harvester': if (job.deposit) textAt("X", main.getDeposit(job.deposit).pos, {color: 'gray', size: 1, lower: .3}); break;
                    case 'repairer': if (job.current) textAt("🔨", Game.getObjectById(job.current).pos, {color: 'white', opacity: .7, size: 1}); break;
                    case 'builder': if (job.site) textAt("🔨", Game.getObjectById(job.site).pos, {color: 'white', opacity: .7, size: 1}); break;
                    case 'dismantler': if (job.target) textAt("🔨", Game.getObjectById(job.target).pos, {color: 'red', opacity: .7, size: 1}); break;
                    case 'extensionFilling':
                        var transferTarget = Game.getObjectById(job.transferTarget);
                        if (transferTarget && transferTarget.maxTransferAmount())
                            textAt("✛", transferTarget.pos, {color: 'white', size: 1, lower: .35});
                    case 'transfer':
                        if (job.deploying) {
                            var transferTarget = Game.getObjectById(job.transferTarget);
                            if (transferTarget)
                                textAt("✛", transferTarget.pos, {color: 'white', size: 1, lower: .35});
                        } else {
                            var withdrawTarget = Game.getObjectById(job.withdrawTarget);
                            if (withdrawTarget)
                                circleAt(withdrawTarget.pos, {fill: 'transparent', radius: 0.65, stroke: '#fff'});
                        }
                    } break;
                case 'miner': /*if (mem.deposit) textAt("⚒️", mem.deposit.pos, {color: 'gray', size: 1});*/ break;
                case 'reserver':
                    var room = Game.rooms[mem.room];
                    var controller = room && room.controller;
                    if (controller) textAt("🏁", controller.pos, {color:
                        controller.owner ? (controller.owner.username == 'Baezon' ? '#0d0' : 'red') : '#0d0', size: .8});
                    break;
            }
            //main.log(creep.pos, creep);
            return true;
        },

        drawInfo: function(creepName) {
            if (!this.orders.drawPath(creepName, true)) return;
            var creep = Game.creeps[creepName];
            //main.log(creep.pos, creep);
            var pos = creep.pos;
            creep.room.visual.text(main.job(creepName), pos.x, pos.y, {});
            return true;
        }
    },

    loop: function() {
        if (typeof RoomVisual == "undefined") return;
        if (!Memory.visual) Memory.visual = [];
        Memory.visual = Memory.visual.filter(order =>
            this.orders[order[0]].call(this, order.slice(1)));
        _.forEach(Game.creeps, creep => {
            this.orders.drawPath(creep.name, main.MILITARY_ROLES.includes(creep.memory.role));
            this.orders.drawJobInfo(creep.name);
        });
        _.forEach(main.hives, hive => {
            var drawChar = (room, lower, color) => new RoomVisual(room).text(
                main.hiveChar(hive.name, lower),24.5,37.8,
                {color, size: 35, opacity: .05});
            drawChar(hive.room.name);
            _.forEach(hive.domain, room => drawChar(room, true, 'white'));
            _.forEach(hive.uncontrols, room => drawChar(room, true, 'red'));
        });
    }
};

Creep.prototype.drawPath = function() {
    Memory.visual.push(['drawPath', this.name, true]);
}
Creep.prototype.drawInfo = function() {
    Memory.visual.push(['drawInfo', this.name]);
}
